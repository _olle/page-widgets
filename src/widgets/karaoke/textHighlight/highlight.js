import { syllable } from 'syllable'
import { getTextNodes, hasText, isHidden } from '../../../utils/DOM-utils/getTextNodes.js'
import { attractBall } from './bouncingball.js'

export const karaokeHighlightId = 'karaoke-highlight'

/**
 * regexp credit: https://stackoverflow.com/a/49407494/17519505
 */
const syllebleRegex = /[^aeiouy]*[aeiouy]+(?:[^aeiouy]*$|[^aeiouy](?=[^aeiouy]))?/gi
const wordRegexString = '\\S+(?:\\s)?'

const wordRegexFlags = 'gi'
const wordRegex = new RegExp(wordRegexString, wordRegexFlags)

const resetRegexp = (regexp) => regexp.lastIndex = 0

const replaceSelection = (txt, replacement, index, cb) =>
    txt.slice(0, index) +
    (cb ? cb(replacement) : replacement) +
    txt.slice(index + replacement.length)

const makeYellow = (sample) => `<span id=${karaokeHighlightId} style="color:black;font-weight:bold;-webkit-text-stroke: 1px yellow;transition:all .2s ease;">${sample}</span>`


export const getHighlightingCallback = () => {
    const filter = node => hasText(node) && !isHidden(node.parentElement) // && (new RegExp(wordRegexString)).test(node.textContent)
    const treeWalker = getTextNodes(document.body, filter)
    let node = treeWalker.nextNode()
    let nextNode = treeWalker.nextNode()
    let clone = node.cloneNode()
    let text = node.textContent
    let syllableCount = 1
    let exec

    return {
        onNoteOn: () => {
            if (exec && syllableCount < syllable(exec[0])) {
                syllableCount += 1
                return
            }

            let lastIndex = wordRegex.lastIndex
            exec = wordRegex.exec(node.textContent)
            syllableCount = 1

            if (!exec) {
                node.replaceWith(clone)
                node = nextNode
                clone = node.cloneNode()
                nextNode = treeWalker.nextNode()
                if (nextNode === null) {
                    return
                }
                text = node.textContent
                resetRegexp(wordRegex)
                lastIndex = 0
                exec = wordRegex.exec(text)
            }

            const txt = document.createElement('span')
            txt.innerHTML = replaceSelection(text, exec[0], lastIndex, makeYellow)
            node.replaceWith(txt)
            node = txt
            attractBall(karaokeHighlightId)
        },
        onEnd: () => {
            console.log('That\'s all folks')
            node.replaceWith(clone)
        }
    }
}
