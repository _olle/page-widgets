import { getContours, waitForOpenCV } from '../../utils/opencv/index.js'
import { aStar, filterOutShapesContainedInOtherShapes, getValidSpawnPointRTree, initRTree } from '../../utils/R-tree/index.js'

self.onmessage = async function (event) {
  const { type, data } = event.data;

  switch (type) {
    case 'init': {
      await waitForOpenCV()
      const contours = getContours(data.imgData)
      initRTree(contours, data.width, data.height)
      const result = filterOutShapesContainedInOtherShapes()
      self.postMessage({ type: 'initiated R-Tree with obstacle polygons', result })
      break
    }

    case 'find place to spawn car': {
      const result = getValidSpawnPointRTree(data.width, data.height, data.bodyWidth, data.bodyHeight, data.padding)
      self.postMessage({ type: 'found place to spawn car', result })
      break
    }

    case 'find place to spawn ghost': {
      const result = getValidSpawnPointRTree(data.width, data.height, data.bodyWidth, data.bodyHeight, data.padding)
      self.postMessage({ type: 'found place to spawn ghost', result })
      break
    }

    case 'find path between ghost and car': {
      const result = aStar(data.from, data.to, data.gridSize)
      self.postMessage({ type: 'found path between ghost and car', result })
      break
    }
  }
}
