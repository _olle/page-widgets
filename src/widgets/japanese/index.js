import { style } from './style'
import { japaneseText } from './japanese_text'
import { changeTo, restore, getProfilePic } from '../../utils/profilepic/profilepic.js'
import { makeSpeechBubble } from '../../utils/speechBubble/speechBubble.js'
import { getNormallyDistributedNumber } from './rnd.js'
import pleaseBeKind from 'url:./pleaseBeKind.gif'
import japaneseOpen from 'url:./me_japanese_open.jpeg'
import japaneseClosed from 'url:./me_japanese_closed.jpeg'
import { play as playJapaneseSpeech } from './japaneseSpeech.js'
import japaneseSpeech from 'url:./japaneseSpeech.mp4'

const styleId = 'japanese-style'
const marqueeId = 'japanese-marquees'
const gifId = 'japanese-gif'
const scriptId = 'japanese-script'

let isOn
let bkgColIntervalId

const isOpen = () => getProfilePic().getAttribute('src') === japaneseOpen
const openMouth = () => changeTo(japaneseOpen)
const closeMouth = () => changeTo(japaneseClosed)

const addStyle = () => {
  const styleElement = document.createElement('style')
  styleElement.textContent = style
  styleElement.id = styleId
  document.head.appendChild(styleElement)
}

const removeAll = () => {
  [marqueeId, gifId, scriptId].forEach(id => {
    document.querySelector('#' + id)?.remove?.()
  })
  isOn = false
  restore()
  clearInterval(bkgColIntervalId)
  window.bkgColInterval && clearInterval(window.bkgColInterval)
  document.querySelector('section').style.backgroundColor = 'white'
  window.location.reload()
}

const makeDiv = (classes, children = []) => {
  const div = document.createElement('div')
  div.classList.add(...classes.split(' '))
  children.forEach(child => div.append(child))
  return div
}

const addMarquees = () => {
  const marqeeText = makeDiv('marquee-text marquee-scroll-2s')
  marqeeText.innerText = japaneseText

  const marqeeTextBig = makeDiv('marquee-text marquee-scroll-3s')
  marqeeTextBig.innerText = japaneseText

  const marquees = makeDiv('marquees', [
    makeDiv('marquee-container', [
      makeDiv('marquee-wrapper', [marqeeText])
    ]),
    makeDiv('marquee-container-big', [
      makeDiv('marquee-wrapper', [marqeeTextBig])
    ])
  ])
  marquees.id = marqueeId
  document.body.appendChild(marquees)
}

const addGif = () => {
  const img = document.createElement('img')
  img.alt = 'Please hire me... or at least take a look?'
  img.src = pleaseBeKind

  const div = makeDiv('please-be-kind', [img])
  div.id = gifId
  document.body.appendChild(div)
}

const changeBackgroundColor = () => {
  const rndCol = () => '#' + Math.random().toString(16).slice(-6)
  const changeBackgroundColor = () => {
    document.body.style.backgroundColor = rndCol()
    document.querySelector('section').style.backgroundColor = rndCol()
  }
  document.body.style.transition = 'all 1s ease'
  document.querySelector('section').style.transition = 'all 1s ease'
  changeBackgroundColor()
  bkgColIntervalId = setInterval(changeBackgroundColor, 1000)
}

const moveMouth = async () => {
  if (!isOn) {
    return
  }
  if (isOpen()) {
    closeMouth()
  } else {
    openMouth()
  }
  setTimeout(moveMouth, getNormallyDistributedNumber(150, 100))
}

const handleButtonClick = (button, soundArrayBuffer) => {
  button.disabled = true
  isOn = true
  addStyle()
  addMarquees()
  addGif()
  moveMouth()
  changeBackgroundColor()
  playJapaneseSpeech(soundArrayBuffer, () => {
    removeAll()
    button.disabled = false
  })
}

const standalone = async () => {
    const button = document.createElement('button')
    button.addEventListener('click', () => handleButtonClick(button))
    button.dispatchEvent(new MouseEvent('click'))
}

// TODO: move initialization to canRun
const forOlleJohannessonDotCom = async () => {
    const response = await fetch(japaneseSpeech)
    const soundArrayBuffer = await response.arrayBuffer()
    const div = document.createElement('div')
    const button = document.createElement('button')
    button.innerHTML = 'よろしくな！<br/>(click me!)'
    button.addEventListener('click', () => handleButtonClick(button, soundArrayBuffer))
    div.appendChild(button)
    await makeSpeechBubble(div)
}

const canRun = async () => true

export { forOlleJohannessonDotCom, standalone, canRun }
