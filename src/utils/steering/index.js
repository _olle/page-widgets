/**
 * The zero vector { x: 0, y: 0 }
 */
export const zeroVector = { x: 0, y: 0 }

/**
 * Add one vector to another
 * @param {{ x: number, y: number }} a - The first vector
 * @param {{ x: number, y: number }} b - The second vector
 * @returns {{ x: number, y: number }} The sum
 */
export function add(a = zeroVector, b = zeroVector) {
  return { x: a.x + b.x, y: a.y + b.y }
}

/**
 * Subtract one vector from another
 * @param {{ x: number, y: number }} a - The first vector
 * @param {{ x: number, y: number }} b - The second vector
 * @returns {{ x: number, y: number }} The difference
 */
export function subtract(a = zeroVector, b = zeroVector) {
  return add(a, multiply(b, -1))
}

/**
 * Multiply a vector with a scalar
 * @param {{ x: number, y: number }} v - The vector
 * @param {number} scalar - The scalar
 * @returns {{ x: number, y: number }} The product
 */
export function multiply(v = zeroVector, scalar = 1) {
  return { x: v.x * scalar, y: v.y * scalar }
}

/**
 * Divide a vector with a scalar
 * @param {{ x: number, y: number }} v - The vector
 * @param {number} scalar - The scalar
 * @returns {{ x: number, y: number }} The quotient
 */
export function divide(v = zeroVector, scalar = 1) {
  return multiply(v, 1 / scalar)
}

/**
 * Magnitude of a vector
 * @param {{ x: number, y: number }} v - The vector
 * @returns {number} The magnitude
 */
export function magnitude(v = zeroVector) {
  return Math.sqrt(v.x * v.x + v.y * v.y)
}

/**
 * Normalize a vector
 * @param {{ x: number, y: number }} v - The vector
 * @returns {{ x: number, y: number }} The normalized vector
 */
export function normalize(v) {
  const mag = magnitude(v)

  return mag > 0
    ? multiply(v, 1 / mag)
    : { x: 0, y: 0 }
  }

/**
 * Limit the magnitude of a vector
 * @param {{ x: number, y: number }} v - The vector
 * @param {number} maxMagnitude  - The magnitude limit
 * @returns {{ x: number, y: number }} The limited Vector
 */
export function limit(v = zeroVector, maxMagnitude) {
  if (maxMagnitude === null || maxMagnitude === undefined) {
    return v
  }

  return setMagnitude(v, Math.min(magnitude(v), maxMagnitude))
}

/**
 * Set the magnitude of a vector
 * @param {{ x: number, y: number }} v - The vector
 * @param {number} magnitude  - The magnitude
 * @returns {{ x: number, y: number }} The resulting vector with adapted magnitude
 */
export function setMagnitude(v = zeroVector, magnitude) {
  if (magnitude === null || magnitude === undefined) {
    return v
  }

  return multiply(normalize(v), magnitude)
}

/**
 * Multiply two vectors
 * @param {{ x: number, y: number }} a - One vector
 * @param {{ x: number, y: number }} b - The other vector
 * @returns {number} The dot product
 */
export function dotProduct(a = zeroVector, b = zeroVector) {
  return a.x * b.x + a.y * b.y
}

/**
 * Distance between two vectors
 * @param {{ x: number, y: number }} a - One vector
 * @param {{ x: number, y: number }} b - The other vector
 * @returns {number} The distance
 */
export function distance(a = zeroVector, b = zeroVector) {
  return magnitude(subtract(a, b))
}

export function angleBetweenVectors(a, b) {
  const dot = dotProduct(a, b)
  const magA = magnitude(a)
  const magB = magnitude(b)

  if (magA === 0 || magB === 0) return 0

  return Math.acos(dot / (magA * magB))
}

/**
 * <pre>
 * {@code
 * Vector Projection
 *      . a
 *     /|
 *    / |
 *   /  |
 * .----.-------. b
 * p    r
 * }
 * </pre>
 * @param {{ x: number, y: number }} position - The base position (p)
 * @param {{ x: number, y: number }} a - The first vector (a)
 * @param {{ x: number, y: number }} b - The second position (b)
 * @returns {{ x: number, y: number }} The projected vector (r)
 */
export function normalPoint(position = zeroVector, a = zeroVector, b = zeroVector) {
  const v1 = subtract(a, position)
  const v2 = normalize(subtract(b, position))
  const sp = dotProduct(v1, v2)
  return add(multiply(v1, sp), position)
}

/**
 * Seek
 * @param {{ x: number, y: number }} position - The current position of the object
 * @param {{ x: number, y: number }} target - The target position
 * @param {{ x: number, y: number }} velocity - The current velocity of the object
 * @param {number} maxSpeed - The maximum speed of the object
 * @param {number} maxForce - The maximum force the object has to change its steering
 * @returns {{ x: number, y: number }} The steering force
 */
export function seek(position, target, velocity, maxSpeed, maxForce = 1) {
  if (!(position && target && velocity && maxSpeed && maxForce)) {
    return zeroVector
  }

  const desiredDirection = subtract(target, position)
  const desiredForce = setMagnitude(desiredDirection, maxSpeed)
  const steering = subtract(desiredForce, velocity)
  return limit(steering, maxForce)
}

/**
 * Doesn't flee away directly from an obstacle, but just slightly brakes and veers to avoid steering right into it.
 *
 * @param {{x: number, y: number}} position - The current position of the object
 * @param {{x: number, y: number}} obstacle - The obstacle position
 * @param {{x: number, y: number}} velocity - The current velocity of the object
 * @param {number} avoidanceRadius - The distance within which the obstacle is "seen"
 * @param {number} maxForce - The maximum force for lateral avoidance and braking
 * @returns {{x: number, y: number}} - The steering force
 */
export function avoidObstacle(position, obstacle, velocity, avoidanceRadius, maxForce, destination) {
  if (!(position && obstacle && velocity && avoidanceRadius && maxForce)) {
    return { x: 0, y: 0 };
  }

  const directionToObstacle = subtract(obstacle, position);
  const distanceToObstacle = magnitude(directionToObstacle);

  if (distanceToObstacle > avoidanceRadius) {
    return { x: 0, y: 0 }
  }

  const forwardDirection = normalize(destination ? subtract(destination, position) : velocity)
  const lateralDirection = { x: -forwardDirection.y, y: forwardDirection.x }
  const sideToSteerAway = dotProduct(forwardDirection, directionToObstacle) > 0 ? -1 : 1
  const urgency = Math.min(1, 1 / (distanceToObstacle + 0.1))
  const brakingFactor = Math.min(1, 1 / (distanceToObstacle * distanceToObstacle + 0.1))
  const lateralForce = multiply(lateralDirection, sideToSteerAway * urgency * maxForce * 0.4)
  const brakingForce = multiply(forwardDirection, -brakingFactor * maxForce * 0.1)

  return add(lateralForce, brakingForce);
}

// export function avoidObstacle(position, obstacle, velocity, avoidanceRadius, maxForce) {
//   if (!(position && obstacle && velocity && avoidanceRadius && maxForce)) {
//     return zeroVector
//   }
//
//   const directionToObstacle = subtract(obstacle, position)
//   const distanceToObstacle = magnitude(directionToObstacle)
//
//   if (distanceToObstacle > avoidanceRadius) {
//     return zeroVector
//   }
//
//   const forwardDirection = normalize(velocity)
//   const lateralDirection = { x: forwardDirection.y * -1, y: forwardDirection.x }
//   const sideToSteerTowards = dotProduct(lateralDirection, directionToObstacle) < 0 ? 1 : -1
//
//   const urgency = 1 / (distanceToObstacle + 0.1)
//   const brakingFactor = Math.min(1, 1 / (distanceToObstacle * distanceToObstacle + 0.1))
//
//   const lateralForce = multiply(lateralDirection, sideToSteerTowards * urgency * maxForce)
//   const brakingForce = multiply(forwardDirection, brakingFactor * maxForce * -1 * 0)
//   return add(lateralForce, brakingForce)
// }


export function avoidObstacles(position, obstacles, velocity, avoidanceRadius, maxForce, destination) {
  if (!(position && obstacles && velocity && avoidanceRadius && maxForce)) {
    return { x: 0, y: 0 };
  }

  const speed = magnitude(velocity)
  velocity = speed ? speed : { x: 3, y: 3 }

  let totalAvoidanceForce = obstacles
    .map(obstacle => avoidObstacle(position, obstacle, velocity, avoidanceRadius, maxForce, destination))
    .reduce(add)

  const forceFactor = Math.min(1, 1 / (speed * speed + 0.1));

  return limit(totalAvoidanceForce, forceFactor);
}

export function stayInBounds(position, velocity, obstacles, avoidanceRadius, maxSpeed, maxForce, target) {
  let totalAvoidanceForce = zeroVector
  let avoidanceNeeded = false
  let targetSteering = multiply(normalize(subtract(target, position)), maxSpeed)

  for (let obstacle of obstacles) {
    const directionToObstacle = subtract(obstacle, position);
    const distanceToObstacle = magnitude(directionToObstacle);

    if (distanceToObstacle < avoidanceRadius) {
      avoidanceNeeded = true;

      const escapeDirection = normalize(multiply(directionToObstacle, -1))
      const escapeVelocity = multiply(escapeDirection, maxSpeed);
      const redirectedEscape = subtract(targetSteering, escapeVelocity)

      const escapeSteer = subtract(redirectedEscape, velocity);
      const escapeForce = limit(escapeSteer, maxForce);

      // const blendedForce = add(multiply(escapeForce, 0.6), multiply(targetSeek, 0.4));

      totalAvoidanceForce = add(totalAvoidanceForce, escapeForce);
    }
  }

  return avoidanceNeeded ? limit(totalAvoidanceForce, maxSpeed) : zeroVector
}

/**
 * Avoid
 * @param {{ x: number, y: number }} position - The current position of the object
 * @param {{ x: number, y: number }} obstacle - The obstacle position
 * @param {{ x: number, y: number }} velocity - The current velocity of the object
 * @param {number} maxSpeed - The maximum speed of the object
 * @param {number} maxForce - The maximum force for lateral avoidance and braking
 * @param {number} fleeRedius - The radius within which the object "sees" the target
 * @returns {{ x: number, y: number }} The steering force
 */
export function flee(position, obstacle, velocity, maxSpeed, maxForce, fleeRedius) {
  if (distance(position, obstacle) > fleeRedius) {
    return zeroVector
  }
  return multiply(seek(position, obstacle, velocity, maxSpeed, maxForce), -1)
}

// export function targetOnPath(position, velocity, path, pathRadius, futureCoefficient = 1) {
//   const pathSegments = [path].flat()
//   const futurePosition = add(multiply(normalize(velocity), futureCoefficient), position)
//   const target = normalPoint(pathSegments[0], futurePosition, pathSegments[pathSegments.length - 1])
//   const targetsDistanceFromFuturePosition = distance(futurePosition, target)
//   return distance(futurePosition, target) > pathRadius
//     ? target
//     :


// }

// export function arrive(position, velocity, target, maxSpeed) {
//   const desired = normalize(subtract(target, position)) // Get direction
//   const desiredVelocity = multiply(desired, maxSpeed) // Scale to max speed
//   return subtract(desiredVelocity, velocity) // Steering force
// }

// export function wander(position, velocity, maxSpeed, maxForce, wanderRadius, wanderDistance, wanderAngle) {
//   const circleCenter = add(position, multiply(velocity, wanderDistance)) // Get the center of the circle
//   const circleTangent = multiply(normalize(velocity), wanderRadius) // Get the tangent of the circle
//   const circleAngle = add(circleTangent, wanderAngle) // Add the angle to the tangent
//   return circleAngle // Return the angle
// }

// export function pursuit(position, velocity, target, maxSpeed) {
//   const desired = normalize(subtract(target, position)) // Get direction
//   const desiredVelocity = multiply(desired, maxSpeed) // Scale to max speed
//   return subtract(desiredVelocity, velocity) // Steering force
// }
