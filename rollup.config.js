import image from '@rollup/plugin-image'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import peerDepsExternal from 'rollup-plugin-peer-deps-external'
import terser from '@rollup/plugin-terser'
import copy from 'rollup-plugin-copy'
import css from 'rollup-plugin-import-css'
import json from '@rollup/plugin-json'
import del from 'rollup-plugin-delete'
import workerLoader from 'rollup-plugin-web-worker-loader'

export default [
  {
    context: 'window',
    input: {
      index: 'src/index.js',
    },
    output: [
      {
        dir: 'dist',
        format: 'es'
      }
    ],
    plugins: [
      del({ targets: 'dist' }),
      image(),
      nodeResolve(),
      css(),
      json(),
      peerDepsExternal(),
      commonjs({
        transformMixedEsModules: true,
        esmExternals: true,
        ignore: ['fs', 'path', 'crypto'], // for @techstark/opencv-js. see: https://github.com/TechStark/opencv-js-examples/blob/develop/opencv-js-rollup-example/rollup.config.js
      }),
      copy({
        targets: [
          {
            src: [
              'src/*.png',
              'src/**/*.png',
              'src/**/*.jpeg',
              'src/**/*.gif',
              'src/**/*.mp4',
              'src/**/*.midi',
              'src/**/*.mid',
              'src/**/*-ogg.js',
              'src/**/*-mp3.js'
            ],
            dest: 'dist'
          },
        ]
      }),
      terser(),
      workerLoader({
        targetPlatform: 'browser',
        inline: true,
        strictExports: true
      })
    ],
  }
]
