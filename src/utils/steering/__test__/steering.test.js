import {
  add,
  subtract,
  multiply,
  divide,
  magnitude,
  normalize,
  limit,
  setMagnitude,
  dotProduct,
  distance,
  normalPoint,
  seek,
  zeroVector
} from '..'

describe('vector math', () => {
  describe('add', () => {
    it('adds one vector to another', () => {
      const northEast = { x: 1, y: 1 }
      const north = { x: 0, y: 1 }
      const expected = { x: 1, y: 2 }
      expect(add(northEast, north)).toEqual(expected)
    })

    it('is commutative', () => {
      const northEast = { x: 1, y: 1 }
      const north = { x: 0, y: 1 }
      const expected = { x: 1, y: 2 }
      expect(add(northEast, north)).toEqual(expected)
    })

    it('arguments default to null vector', () => {
      expect(add()).toEqual({ x: 0, y: 0 })
    })
  })
  describe('subtract', () => {
    it('subtracts one vector from another', () => {
      const northEast = { x: 1, y: 1 }
      const north = { x: 0, y: 1 }
      const east = { x: 1, y: 0 }
      expect(subtract(northEast, north)).toEqual(east)
    })

    it('the second argument is subtracted from the first', () => {
      const northEast = { x: 1, y: 1 }
      const north = { x: 0, y: 1 }
      const west = { x: -1, y: 0 }
      expect(subtract(north, northEast)).toEqual(west)
    })

    it('arguments default to null vector', () => {
      expect(subtract({ x: 1, y: 1 })).toEqual({ x: 1, y: 1 })
      expect(subtract()).toEqual({ x: 0, y: 0 })
    })
  })

  describe('multiply', () => {
    it('multiplies a vector with a scalar', () => {
      expect(multiply({ x: 1, y: 1 }, 2)).toEqual({ x: 2, y: 2 })
      expect(multiply({ x: 1, y: 1 }, -2)).toEqual({ x: -2, y: -2 })
      expect(multiply({ x: 1, y: 1 }, 1.5)).toEqual({ x: 1.5, y: 1.5 })
    })
    it('scalar defaults to 1', () => {
      expect(multiply({ x: 1, y: 1 })).toEqual({ x: 1, y: 1 })
    })

    it('vector defaults to null vector', () => {
      expect(multiply()).toEqual({ x: 0, y: 0 })
    })
  })


  describe('divide', () => {
    it('divide a vector with a scalar', () => {
      expect(divide({ x: 1, y: 1 }, 2)).toEqual({ x: 1/2, y: 1/2 })
      expect(divide({ x: 1, y: 1 }, -2)).toEqual({ x: -1/2, y: -1/2 })
      expect(divide({ x: 1, y: 1 }, 1.5)).toEqual({ x: 2/3, y: 2/3 })
    })
    it('scalar defaults to 1', () => {
      expect(divide({ x: 1, y: 1 })).toEqual({ x: 1, y: 1 })
    })

    it('vector defaults to null vector', () => {
      expect(divide()).toEqual({ x: 0, y: 0 })
    })
  })

  describe('magnitude', () => {
    it('returns the magnitude of a vector', () => {
      expect(magnitude({ x: 3, y: 4 })).toEqual(5)
    })
    it('its an absolute value', () => {
      expect(magnitude({ x: -3, y: -4 })).toEqual(5)
    })

    it('vector defaults to null vector', () => {
      expect(magnitude()).toEqual(0)
    })
  })


  describe('normalize', () => {
    it('normalizes a vector', () => {
      expect(normalize({ x: 3, y: 3 }).x).toBeCloseTo(0.7071)
      expect(normalize({ x: 3, y: 3 }).y).toBeCloseTo(0.7071)
    })

    it('respects direction', () => {
      expect(normalize({ x: -3, y: 3 }).x).toBeCloseTo(-0.7071)
      expect(normalize({ x: -3, y: 3 }).y).toBeCloseTo(0.7071)
      expect(normalize({ x: 3, y: -3 }).x).toBeCloseTo(0.7071)
      expect(normalize({ x: 3, y: -3 }).y).toBeCloseTo(-0.7071)
    })

    it('if the magnitude is 0, it returns the 0 vector', () => {
      expect(normalize({ x: 0, y: 0 })).toEqual({ x: 0, y: 0 })
    })

    it('vector defaults to null vector', () => {
      expect(normalize()).toEqual({ x: 0, y: 0 })
    })
  })


  describe('limit', () => {
    it('limits a vector to a max magnitude', () => {
      expect(limit({ x: 3, y: 3 }, 2).x).toBeCloseTo(1.4142)
      expect(limit({ x: 3, y: 3 }, 2).y).toBeCloseTo(1.4142)
    })

    it('vectors with a smaller magnitude are not affected', () => {
      expect(limit({ x: 3, y: 3 }, 5)).toEqual({ x: 3, y: 3 })
    })

    it('respects direction', () => {
      expect(limit({ x: -3, y: 3 }, 2).x).toBeCloseTo(-1.4142)
      expect(limit({ x: -3, y: 3 }, 2).y).toBeCloseTo(1.4142)

      expect(limit({ x: 3, y: -3 }, 2).x).toBeCloseTo(1.4142)
      expect(limit({ x: 3, y: -3 }, 2).y).toBeCloseTo(-1.4142)

      expect(limit({ x: -3, y: -3 }, 2).x).toBeCloseTo(-1.4142)
      expect(limit({ x: -3, y: -3 }, 2).y).toBeCloseTo(-1.4142)
    })

    it('magnitude defaults to the magnitude of the vector', () => {
      expect(limit({ x: -3, y: 3 })).toEqual({ x: -3, y: 3 })
      expect(limit({ x: 3, y: -3 })).toEqual({ x: 3, y: -3 })
      expect(limit({ x: -3, y: -3 })).toEqual({ x: -3, y: -3 })
    })

    it('vector defaults to null vector', () => {
      expect(limit()).toEqual({ x: 0, y: 0 })
    })
  })

  describe('setMagnitude', () => {
    it('if the magnitude of a vector is too low it adjusts it upwards', () => {
      expect(setMagnitude({ x: 3, y: 3 }, 10).x).toBeCloseTo(7.071)
      expect(setMagnitude({ x: 3, y: 3 }, 10).y).toBeCloseTo(7.071)
    })

    it('if the magnitude of a vector is too high it adjusts it downwards', () => {
      expect(setMagnitude({ x: 3, y: 3 }, 2).x).toBeCloseTo(1.4142)
      expect(setMagnitude({ x: 3, y: 3 }, 2).y).toBeCloseTo(1.4142)
    })

    it('the magnitude can be 0', () => {
      expect(setMagnitude({ x: 3, y: 3 }, 0)).toEqual({ x: 0, y: 0 })
    })

    it('the magnitude can be negative', () => {
      expect(setMagnitude({ x: 3, y: 4 }, -5).x).toBeCloseTo(-3)
      expect(setMagnitude({ x: 3, y: 4 }, -5).y).toBeCloseTo(-4)

      expect(setMagnitude({ x: -3, y: 4 }, -5).x).toBeCloseTo(3)
      expect(setMagnitude({ x: -3, y: 4 }, -5).y).toBeCloseTo(-4)
    })

    it('respects direction', () => {
      expect(setMagnitude({ x: -3, y: 3 }, 2).x).toBeCloseTo(-1.4142)
      expect(setMagnitude({ x: -3, y: 3 }, 2).y).toBeCloseTo(1.4142)

      expect(setMagnitude({ x: 3, y: -3 }, 2).x).toBeCloseTo(1.4142)
      expect(setMagnitude({ x: 3, y: -3 }, 2).y).toBeCloseTo(-1.4142)

      expect(setMagnitude({ x: -3, y: -3 }, 2).x).toBeCloseTo(-1.4142)
      expect(setMagnitude({ x: -3, y: -3 }, 2).y).toBeCloseTo(-1.4142)
    })
+
    it('if the magnitude is not defined, it returns the vector', () => {
      expect(setMagnitude({ x: 1, y: 2 })).toEqual({ x: 1, y: 2 })
    })

    it('vector defaults to null vector', () => {
      expect(setMagnitude()).toEqual({ x: 0, y: 0 })
    })
  })

  describe('dotProduct', () => {
    it('calculates the dot product of two vectors', () => {
      expect(dotProduct({ x: 2, y: 2 }, { x: 3, y: 4 })).toEqual(14)
      expect(dotProduct({ x: 2, y: -2 }, { x: 3, y: 4 })).toEqual(-2)
      expect(dotProduct({ x: -2, y: 2 }, { x: 3, y: 4 })).toEqual(2)
      expect(dotProduct({ x: -2, y: -2 }, { x: 3, y: 4 })).toEqual(-14)
    })

    it('both vectors default to the null vector', () => {
      expect(dotProduct({ x: 2, y: 2 })).toEqual(0)
      expect(dotProduct()).toEqual(0)
    })
  })

  describe('distance', () => {
    it('measure the absolute distance between two vectors', () => {
      expect(distance({ x: 1, y: 2 }, { x: -2, y: -2 })).toEqual(5)
    })
  })
})


describe('steering', () => {
  it('stays if target is reached', () => {
    const ones = { x: 1, y: 1 }
    const force = seek(ones, ones, zeroVector, 1, 1)
    expect(force).toEqual({ x: 0, y: 0 })
  })

  it('if going north-east and the target is east, the steering vector should be south', () => {
    const origin = { x: 0, y:0 }
    const northEast = { x: 10, y: 10 }
    const east = { x: 20, y: 0 }
    const southEast = { x: 0, y: -1 }

    const force = seek(origin, east, northEast, 10, 1)
    expect(force).toEqual(southEast)
  })
})