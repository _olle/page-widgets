export const closeTo = (x, y, tolerance) => (x > (y - tolerance ?? Math.log(x))) && (x < (y + tolerance ?? Math.log(x)))

export const compareArrayWithTolerance = (a, b, tolerance) => {
  for (let i = 0, l = a.length; i < l; i++) {
    if (!(closeTo(a[i], b[i], tolerance))) {
      return false
    }
  }
  return true
}
