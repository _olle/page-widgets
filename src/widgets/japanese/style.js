export const style = `
  .marquees {
      position: absolute;
      top: 0;
      width: 100%;
  }

  .marquee-container {
      width: 100%;
      overflow: hidden;
      white-space: nowrap;
      background-color: #222;
      color: #fff;
      padding: 10px 0;
      font-size: 24px;
      font-weight: bold;
      font-family: 'Arial', sans-serif;
      position: relative;
  }

  .marquee-container-big {
      width: 100%;
      overflow: hidden;
      white-space: nowrap;
      background-color: hotpink;
      color: yellow;
      padding: 10px 0;
      font-size: 60px;
      font-weight: bold;
      font-family: 'Arial', sans-serif;
      position: relative;
  }

  .marquee-wrapper {
      display: flex;
      width: 200%;
  }

  .marquee-text {
      display: inline-block;
      width: 100%;
  }

  .marquee-scroll-2s {
      animation: marquee-scroll 2s linear infinite;
  }

  .marquee-scroll-3s {
      animation: marquee-scroll 3s linear infinite;
  }

  @keyframes marquee-scroll {
      from { transform: translateX(0%); }
      to { transform: translateX(-50%); }
  }

  .please-be-kind {
      position: absolute;
      bottom: 0px;
      left: 0px;
      width: 22em;
      height: auto;
  }
  `
