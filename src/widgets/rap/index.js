import { getTextNodes, hasText, isHidden } from '../../utils/DOM-utils/getTextNodes.js'
import cursor from './rap_cursor.png'
import cursorActive from './cursor-active.png'
import { rap, stop, isPlaying } from './rap.js'
import EasySpeech from 'easy-speech'
import { makeSpeechBubble } from '../../utils/speechBubble/speechBubble.js'
import speechBubbleText from 'url:./speechBubbleText.png'

const onFail = () => {
    window.location.reload()
}

const init = () => {
    return EasySpeech.init({ maxTimeout: 2000, interval: 250 })
        .then(duThing)
        .catch(onFail)
}

const onClickText = (event) => {
    event.stopPropagation()
    rap(event.target.textContent)
}

const onClickAway = () => {
    if (isPlaying) {
        stop()
    }
}

const duThing = () => {
    const filter = node => hasText(node) && !isHidden(node.parentElement) // && (new RegExp(wordRegexString)).test(node.textContent)
    const treeWalker = getTextNodes(document.body, filter)
    let node

    const style = document.createElement('style')
    document.head.appendChild(style)
    const className = 'rap-text-node'
    style.appendChild(document.createTextNode(`
        .${className}:hover {
            cursor: url(${cursor}), auto;
        }
        .${className}:hover:active {
            cursor: url(${cursorActive}), auto;
        }
    `))

    // several text nodes can have the same parent element
    // so we need to keep track of the last parent element
    let lastParent = undefined

    while (node = treeWalker.nextNode()) {
        const currentParent = node.parentElement
        if (lastParent !== currentParent) {
            currentParent.classList.add(className)
            currentParent.addEventListener('click', onClickText)
            lastParent = currentParent
        }
    }
    document.body.addEventListener('click', onClickAway)
}

const forOlleJohannessonDotCom = () => init().then(() => {
    const img = document.createElement('img')
    img.alt = 'Click any text and rap'
    img.src = speechBubbleText
    img.style.width = '10em'
    img.style.height = 'auto'
    makeSpeechBubble(img)
})

const standalone = init

const canRun = async () => {
    return EasySpeech.init({ maxTimeout: 2000, interval: 250 })
        .then(EasySpeech.reset)
        .then(() => true, () => false)
}

export { forOlleJohannessonDotCom, standalone, canRun }
