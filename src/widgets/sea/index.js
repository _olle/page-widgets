// see below :(
// import Bowser from 'bowser'

import sea1080p from 'url:./sea-1080p.mp4'
import sea720p from 'url:./sea-720p.mp4'
import sea480p from 'url:./sea-480p.mp4'

const run = () => {
  const profilePicture = document.getElementById('profile-picture')
  // safari sometimes has problems with mix-blend-mode, no solution found yet
  // except using a canvas, and it's juuust not worth it
  profilePicture.style.setProperty('mix-blend-mode', 'color')
  const profilePictureYPosition = profilePicture.getBoundingClientRect().y
  const profilePictureHeight = profilePicture.getBoundingClientRect().height

  const videoTag = document.createElement('video')
  videoTag.autoplay = true
  videoTag.muted = true
  videoTag.loop = true
  // iOS special case
  // If this doesnt work, we can force it with javascript see below.
  // note that this property is camelcased in javascript (unlike autoplay...)
  videoTag.playsInline = true

  // sticky positioning instead of absolute positioning
  // In some browsers with very narrow screens the page actually can scroll, 
  // and the video should stay on top of the profile picture
  videoTag.style.setProperty('position', 'sticky')
  videoTag.style.setProperty('top', `${profilePictureYPosition}px`)
  videoTag.style.setProperty('width', '100%')
  videoTag.style.setProperty('min-height', `${profilePictureHeight}px`)
  videoTag.style.setProperty('object-fit', 'cover')
  videoTag.style.setProperty('filter', 'invert(1)')
  // hide video at first until we have mounted it to the DOM and can calculate the height
  videoTag.style.setProperty('visibility', 'hidden')

  const videoSrcLarge = document.createElement('source')
  videoSrcLarge.setAttribute('src', sea1080p)
  videoSrcLarge.setAttribute('type', 'video/mp4')
  videoSrcLarge.setAttribute('media', '(min-width: 1200px)')
  videoTag.appendChild(videoSrcLarge)

  const videoSrcMedium = document.createElement('source')
  videoSrcMedium.setAttribute('src', sea720p)
  videoSrcMedium.setAttribute('type', 'video/mp4')
  videoSrcMedium.setAttribute('media', '(min-width: 768px)')
  videoTag.appendChild(videoSrcMedium)

  const videoSrcDefault = document.createElement('source')
  videoSrcDefault.setAttribute('src', sea480p)
  videoSrcDefault.setAttribute('type', 'video/mp4')
  videoTag.appendChild(videoSrcDefault)

  const contentWrapper = document.createElement('div')
  contentWrapper.style.setProperty('position', 'absolute')
  contentWrapper.style.setProperty('top', '0px')
  // contentWrapper.style.setProperty('mix-blend-mode', 'hard-light')

  while (document.body.firstChild) {
    contentWrapper.appendChild(document.body.firstChild)
  }

  document.body.appendChild(contentWrapper)
  document.body.insertBefore(videoTag, document.body.firstChild)

  // wait for the video to load and then reposition it slightly
  window.requestAnimationFrame(() => {    
    const videoHeight = videoTag.getBoundingClientRect().height
    videoTag.style.setProperty('top', `${profilePictureYPosition - videoHeight / 2}px`)
    videoTag.style.setProperty('visibility', 'visible')

    // If it comes to it... This hack might hack it.
    // if (Bowser.getParser(window.navigator.userAgent).isOS('ios')) {
    //   videoTag.play()
    // }
  })
}

const forOlleJohannessonDotCom = run
const standalone = run
const canRun = () => true

export { forOlleJohannessonDotCom, standalone, canRun }
