// import MIDI from './midi.js'
import * as MIDI from 'midicube'
import { debounce } from 'debounce'
import eternalFlame from 'url:./eternal.mid'
import { makeSpeechBubble } from '../../utils/speechBubble/speechBubble.js'
import { getHighlightingCallback } from './textHighlight/highlight.js'

let player
const startText = '♫ ⏵'
const pauseText = '⏸'

const initializeMIDI = () => new Promise((resolve, reject) => {
    window.MIDI = MIDI
    player = new MIDI.Player()
    const onError = () => reject(new Error('could not load sound font'))
    const onSuccess = () => resolve(player)

    MIDI.loadPlugin({
        instruments: ['acoustic_grand_piano', 'bright_acoustic_piano'],
        soundfontUrl: 'https://gleitz.github.io/midi-js-soundfonts/FatBoy/',
        onprogress: () => console.info('loading sound font...'),
        onsuccess: onSuccess,
        onerror: onError
    })
})

const loadMidiFile = (player) => new Promise((resolve, reject) => {
    player.timeWarp = 1.25
    const { onNoteOn, onEnd } = getHighlightingCallback()
    attachNoteOnListener(player, onNoteOn, onEnd)
    player.on
    player.loadFile(
        eternalFlame,
        () => resolve(player),
        () => {},
        () => reject(new Error('failed to load MIDI file'))
    )
})

const attachNoteOnListener = (player, cb, onEnd) => {
    player.addListener(data => {
        const expectedMelodyChannel = 0
        const noteOnMessage = 144 // 128 is noteOff, 144 is noteOn
        if(data.message === noteOnMessage) {
            debounce(onEnd, 5000)
            if(data.channel === expectedMelodyChannel) {
                cb()
            }
        }
    })
}

const getPlayer = (button) => new Promise((resolve) => {
    if (!player) {
        button.disabled = true
        button.innerHTML = '…'
        initializeMIDI()
            .then(loadMidiFile)
            .then(resolve)
            .catch(console.error)
    } else {
        resolve(player)
    }
})

const handleButtonClick = async (button) => {
    const player = await getPlayer(button)
    button.disabled = false
    if (player.playing) {
        player.pause()
        button.innerHTML = startText
    } else {
        player.start()
        button.innerHTML = pauseText
    }
}

// TODO: move initialization to canRun
const standalone = async () => {
    const button = document.createElement('button')
    button.addEventListener('click', () => handleButtonClick(button))
    button.dispatchEvent(new MouseEvent('click'))
}

// TODO: move initialization to canRun
const forOlleJohannessonDotCom = async () => {
    const div = document.createElement('div')
    const button = document.createElement('button')
    button.innerHTML = startText
    button.addEventListener('click', () => handleButtonClick(button))
    div.appendChild(button)
    await makeSpeechBubble(div)
}

// There is something wrong with playing the MIDI file on iOS
// Perhaps a different MIDI library is needed?
const canRun = (bowser) => !bowser.isOS('ios')

export { forOlleJohannessonDotCom, standalone, canRun }

