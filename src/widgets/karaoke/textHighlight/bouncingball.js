import cssText from './bouncingball.css'
const bouncingBallId = 'karaoke-bouncing-ball'

export const attractBall = (targetId) => {
    let style
    let bouncingBall = document.getElementById(bouncingBallId)
    if (!bouncingBall) {
        const css = JSON.stringify(cssText)
            .replace(/\\n/g, '')
            .replaceAll('"', '')
        style = document.createElement('style')
        document.head.appendChild(style)
        style.appendChild(document.createTextNode(css))

        bouncingBall = document.createElement('div')
        bouncingBall.id = bouncingBallId
        document.body.appendChild(bouncingBall)
    }
    const highlight = document.getElementById(targetId)
    if (!highlight) {
        bouncingBall.style.visibility = 'hidden'
        bouncingBall.remove()
        style.remove()
    }
    const { width, height, x, y } = highlight.getBoundingClientRect()
    bouncingBall.style.top = (y - height / 2 + window.scrollY) + 'px'
    bouncingBall.style.left = x + width / 2 + 'px'
}