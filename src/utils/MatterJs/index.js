import Matter from 'matter-js'
import Shape from '@doodle3d/clipper-js'

/**
 * Convert a contour to a clipperjs shape
 * @param {Array<{x: number, y: number}> } contour
 * @returns {Shape}
 *
 * Shape: {@link https://github.com/Doodle3D/clipper-js?tab=readme-ov-file#readme}
 */
export const toClipperJsShape = contour => new Shape([contour], true, true, true, true)
  .clean(0.1)
  .fixOrientation()

/**
 * Check if a shape is not the perimeter of the image
 * @param {Shape} shape
 * @param {number} width
 * @param {number} height
 * @returns {boolean}
 *
 * Shape: {@link https://github.com/Doodle3D/clipper-js?tab=readme-ov-file#readme}
 */
export const isNotImagePerimeter = (shape, width, height) => {
  // opencv might provide the perimeter of the entire image as a contour, so we filter out anything that is too big
  return Math.abs(shape.totalArea()) < (width - 10) * (height - 10)
}

/**
 * @param {Shape} shape - see: {@link https://github.com/Doodle3D/clipper-js?tab=readme-ov-file#readme}
 * @returns {{shapeBounds: { left: number, right: number, top: number, bottom: number }, path: Array<{x: number, y: number}>}}
 */
export function toJSON(shape) {
  return {
    shapeBounds: shape.shapeBounds(),
    path: shape.paths[0].map(path => ({ x: path.X, y: path.Y }))
  }
}

/**
 *
 * @param {{shapeBounds: { left: number, right: number, top: number, bottom: number }, path: Array<{x: number, y: number}>}} shapeJSON
 * @returns {body} Matter.js Body
 */
export function toMatterJsBody(shapeJSON) {
  const { left, top } = shapeJSON.shapeBounds
  const body =  Matter.Bodies.fromVertices(left, top, shapeJSON.path, { isStatic: true }, false, 0.01, 0, 0.01)
  // The position I have provided is the top left corner of the bounding box of the shapeJSON.
  // But Matter.js calculates the center of mass of the body and positions it using that vector,
  // so the body is slightly offset upwards and to the left.
  // When we fix this, we have to consider that the position we're offsetting is some kind of
  // center point, and we dont know exactly what it is, so we have to reposition relative to it.
  const deltaX = left - body.bounds.min.x
  const deltaY = top - body.bounds.min.y
  const correctedPosition = {
    x: body.position.x + deltaX,
    y: body.position.y + deltaY
  }
  Matter.Body.setPosition(body, correctedPosition)
  return body
}
