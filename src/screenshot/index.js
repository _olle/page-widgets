import html2canvas from 'html2canvas'

/**
 * N.B: Uses scale = 1
 *
 * Retina displays and others have a higher pixel ratio (check window.devicePixelRatio).
 * That might cause the image output to be scaled wrong. Setting the scale to 1
 * fixes that.
 *
 * Yes, I could have just adjusted for it with opencv later on, (scaling the image down
 * by the same factor), but this is simpler and works well enough.
 *
 * @param {Element} target
 * @returns {Promise<ImageData>}
 */
export async function takeScreenshot(target = document.body) {
  const canvas = await html2canvas(target, { scale: 1, })
  const ctx = canvas.getContext('2d', { willReadFrequently: true })
  const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height)
  console.debug(imageData)
  return imageData
}
