import cv from '@techstark/opencv-js'

export async function waitForOpenCV() {
  return new Promise((resolve) => {
    if (cv.getBuildInformation) {
      resolve()
    } else {
      cv.onRuntimeInitialized = () => {
        resolve()
      }
    }
  })
}

export function scaleToDevicePixelRatio(img) {
  if (window.devicePixelRatio > 1 ) {
    const scaledSize = new cv.Size(
      img.cols / window.devicePixelRatio,
      img.rows / window.devicePixelRatio)
    cv.resize(img, img, scaledSize, 0, 0, cv.INTER_AREA)
  }
  return img
}

export function scaleToSize(img, width, height) {
  const scaledSize = new cv.Size(width, height)
  cv.resize(img, img, scaledSize, 0, 0, cv.INTER_AREA)
  return img
}

export function cannnyFilterImageData(imageData, height, width) {
  height ??= imageData.height
  width ??= imageData.width
  console.log('height', height, 'width', width)
  // load the image data into a cv.Mat
  const dest = new cv.Mat(imageData.height, imageData.width, cv.CV_8UC4)
  dest.data.set(imageData.data)

  // Convert to grayscale
  cv.cvtColor(dest, dest, cv.COLOR_RGBA2GRAY)

  // Apply a light Gaussian blur
  cv.GaussianBlur(dest, dest, new cv.Size(3, 3), 0.6)

  // Apply *Canny first* (no thresholding before this)
  cv.Canny(dest, dest, 80, 180, 3, true)

  // apply thresholding to solidify lines
  cv.threshold(dest, dest, 100, 255, cv.THRESH_BINARY)

  // Use morphology only if edges look broken
  const kernel = cv.Mat.ones(2, 2, cv.CV_8U)
  cv.morphologyEx(dest, dest, cv.MORPH_CLOSE, kernel)
  return dest
}

/**
 * Get the contours of the image data as an array of arrays of points
 * @param {ImageData} imageData
 * @returns {Array<Array<{x: number, y: number}>>}
 */
export function getContours(imageData) {
  // load the image data into a cv.Mat
  const dest = new cv.matFromImageData(imageData)
  // const dest = new cv.Mat(imageData.height, imageData.width, cv.CV_8UC4)
  // dest.data.set(imageData.data)

  // convert to grayscale
  // these things are always done on single channel images
  // if you think about it, what would color edge finding find?
  cv.cvtColor(dest, dest, cv.COLOR_RGBA2GRAY, 0)

  // this makes things a bit clearer
  cv.threshold(dest, dest, 120, 200, cv.THRESH_BINARY)

  // find the contours
  const contours = new cv.MatVector()
  const hierarchy = new cv.Mat()
  cv.findContours(dest, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)

  // approximates each contour to polygon
  // what this means is that we can slightly simplify the shape
  // and "close" it. This gets us matrices, so we have
  // to convert them back into arrays afterwards
  const polygonCoordinateArrays = []
  for (let i = 0; i < contours.size(); ++i) {
    const polygonApproximation = new cv.Mat()
    const contour = contours.get(i)
    cv.approxPolyDP(contour, polygonApproximation, 0, true)

    // The result is a matrix, so we have to convert it to an array
    const polygonCoordinateArray = matToCoordinateArray(polygonApproximation)

    // We can take this opportunity to filter out tiny shapes
    // that are probably just noise
    if (polygonCoordinateArray.length > 3) {
      polygonCoordinateArrays.push(polygonCoordinateArray)
    }

    contour.delete()
    polygonApproximation.delete()
  }

  dest.delete()
  contours.delete()
  hierarchy.delete()
  return polygonCoordinateArrays
}

/**
 * Convert a cv.Mat to an array of points
 * The data ist stored in a wierdo opencv type,
 * this sorts it into a primitive array
 *
 * @param {cv.Mat} mat
 * @returns {Array<{x: number, y: number}>}
 */
function matToCoordinateArray(mat) {
  const data = mat.data32S  // typed array of [x0, y0, x1, y1, ...]
  const points = []
  for (let i = 0; i < data.length; i += 2) {
    points.push({ x: data[i], y: data[i + 1] })
  }
  return points
}

/**
 * Get the upward facing edges of the image data
 * This will be useful for the flocking module.
 * Probably needs a bit of tweeking, it seems to
 * me that it finds the downward facing edges...
 * @param {*} imageData
 * @returns
 */
export function getUpwardFacingEdges(imageData) {
  // load the image data into a cv.Mat
  const dest = new cv.Mat(imageData.height, imageData.width, cv.CV_8UC4)
  dest.data.set(imageData.data)

  // Convert to grayscale
  cv.cvtColor(dest, dest, cv.COLOR_RGBA2GRAY)

  // Apply a light Gaussian blur
  cv.GaussianBlur(dest, dest, new cv.Size(3, 3), 0.8)

  //TODO: check that this is not the downward facing edges
  cv.Sobel(dest, dest, cv.CV_8U, 0, 1, 3, 1, 0, cv.BORDER_DEFAULT)

  //apply thresholding to solidify lines
  cv.threshold(dest, dest, 100, 255, cv.THRESH_BINARY)

  return scaleToDevicePixelRatio(dest)
}

export function getBrightPixels(mat, threshold) {
  // Threshold to create a binary mask
  const binary = new cv.Mat()
  cv.threshold(mat, binary, threshold, 255, cv.THRESH_BINARY)

  const { cols } = binary
  const data = binary.data // Typed array (Uint8Array if CV_8U)
  const coordinates = []

  // data.length = rows * cols for a single-channel matrix
  for (let i = 0; i < data.length; i++) {
    if (data[i] > 0) {
      // Convert 1D index (i) to 2D coords (x, y).
      const x = i % cols
      const y = (i / cols) | 0 // integer division

      // If you want x scaled by 4, do it here:
      coordinates.push({ x: x - 2, y: y - 2 })
    }
  }

  // Cleanup
  binary.delete()
  return coordinates
}

/**
 * Get the bright pixels of the image data
 * This is useful for extracting edges after
 * using a sobel or canny filter
 *
 * @param {cv.Mat} mat
 * @param {number} threshold
 * @returns {Array<{x: number, y: number}>}
 */
export function getBrightPixelsOld(mat, threshold) {
  const binary = new cv.Mat()

  cv.threshold(mat, binary, threshold, 255, cv.THRESH_BINARY) // Convert to binary (0 or 255)

  const coordinates = []
  for (let x = 0; x < binary.cols; x++ ) {
    for (let y = 0; y < binary.rows; y++) {
      if (binary.intAt(y, x) > 0) {
        coordinates.push({ x: x * 4, y })
      }
    }
  }
  binary.delete()
  return coordinates
}

export function matToImageData(mat) {
  // 1. Convert to RGBA if needed:
  // If mat is CV_8UC3 (BGR), convert to RGBA:
  // If mat is CV_8UC1 (grayscale), convert to RGBA:
  // If mat is CV_8UC4 (BGRA), convert to RGBA, etc.

  const rgbaMat = new cv.Mat()
  switch (mat.type()) {
    case cv.CV_8UC1: {
      cv.cvtColor(mat, rgbaMat, cv.COLOR_GRAY2RGBA)
      break
    }
    case cv.CV_8UC3: {
      cv.cvtColor(mat, rgbaMat, cv.COLOR_BGR2RGBA)
      break
    }
    case cv.CV_8UC4: {
      cv.cvtColor(mat, rgbaMat, cv.COLOR_BGRA2RGBA)
      break
    }
    default: {
      throw new Error(`Unsupported Mat type: ${mat.type()}`)
    }
  }

    // 2. Make an ImageData object from the RGBA Mat
    const data = new Uint8ClampedArray(rgbaMat.data)
    const imageData = new ImageData(data, rgbaMat.cols, rgbaMat.rows)

    rgbaMat.delete()
    return imageData
  }

export function matToGrayscaleImageData(mat) {
  // Ensure the Mat is single-channel grayscale (CV_8UC1)
  if (mat.type() !== cv.CV_8UC1) {
    throw new Error('Input Mat must be grayscale (CV_8UC1)')
  }

  const width = mat.cols
  const height = mat.rows
  const size = width * height
  const rgbaData = new Uint8ClampedArray(size * 4)

  for (let i = 0; i < size; i++) {
    const grayscaleValue = mat.data[i]
    rgbaData[i * 4] = grayscaleValue
    rgbaData[i * 4 + 1] = grayscaleValue
    rgbaData[i * 4 + 2] = grayscaleValue
    rgbaData[i * 4 + 3] = 255
  }

  return new ImageData(rgbaData, width, height)
}


