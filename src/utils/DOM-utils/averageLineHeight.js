export function getAverageLineHeight() {
  const defaultLineHeight = 16  
  const elements = [...document.querySelectorAll('*')]
  const lineHeights = elements
      .map(el => parseFloat(window.getComputedStyle(el).lineHeight))
      .filter(lh => !isNaN(lh) && lh > 0)

  return lineHeights.length > 0 
      ? Math.round(lineHeights.reduce((sum, lh) => sum + lh, 0) / lineHeights.length) 
      : defaultLineHeight
}
