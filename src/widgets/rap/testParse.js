import { syllable } from 'syllable'
import { hyphenated } from 'hyphenated'
import { chunk } from '../../utils/jshelpers'

export const COMMA = ','

export const DOT = '.'

export const SPACE = ' '

export const capture = chars => new RegExp(`([${chars.join('')}])`)

export const retainCommaAndDot = capture([COMMA, DOT])

export const splitOnSpace = text => text.split(SPACE)

export const noFalsyValues = value => Boolean(value)

export const replaceWithSpace = char => text => [text].flat().map(t => t.replaceAll(char, SPACE))

export const replaceDotsWithSpace = replaceWithSpace(DOT)

export const replaceCommaWithSpace = replaceWithSpace(COMMA)

export const splitOnWordsButRetainSeparators = text => text
    .split(retainCommaAndDot)
    .map(splitOnSpace)
    .flat()
    .filter(noFalsyValues)

export const replaceSeparatorsWithSpace = text => replaceCommaWithSpace(replaceDotsWithSpace(text))

export const breakUpSyllables = (text) => {
  const arrayText = [text].flat()
  const hasMultipleSyllables = word => syllable(word) > 1

  return arrayText
    .map(word => hasMultipleSyllables(word) ? hyphenated(word).split(/\u00AD/g) :[word])
    .flatMap(word => chunk(word, 4))
}
