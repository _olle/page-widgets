import cssText from './speechBubble.css'

export const makeSpeechBubble = async (content, timeout) => {
  // this is sad, but it has to do with parcel and the static file problem...
  // add CSS to head
  const css = JSON.stringify(cssText)
    .replace(/\\n/g, '')
    .replaceAll('"', '')
  const style = document.createElement('style')
  document.head.appendChild(style)
  style.appendChild(document.createTextNode(css))

  // make speech bubble and add content
  const speechBubble = document.createElement('div')
  speechBubble.classList.add('speech-bubble')
  speechBubble.appendChild(content)

  // find element to position next to
  const img = document.querySelector('#profile-picture')
  if (!img) {
    console.error('could not find profile image')
  }

  const position = () => {
    // wait for CSS paints and position speechbubble absolutely next to img
    window.requestAnimationFrame(() => {
      if (window.innerWidth > 480) {
        // speech bubble to the left of the image 
        speechBubble.style.top = img.offsetTop + (img.clientHeight / 2) - (speechBubble.clientHeight / 2) + 'px'
        speechBubble.style.left = img.offsetLeft - speechBubble.clientWidth - 50 + 'px'
      } else {
        // speech bubble above the image
        speechBubble.style.top = img.offsetTop - speechBubble.clientHeight - 30 + 'px'
        speechBubble.style.left = img.offsetLeft + img.clientWidth / 2 - speechBubble.clientWidth / 2 + 'px'
      }
    })
  }

  // add speechbubble to dom tree and make sure the positioning is true
  position()
  document.body.appendChild(speechBubble)
  window.addEventListener('resize', position)

  if (!timeout) {
    return
  }

  // fadeout
  const remove = () => {
    speechBubble.classList.add('fadeout')
    speechBubble.ontransitionend = () => speechBubble.remove()
  }

  if (typeof timeout === 'number') {
    setTimeout(remove, timeout)
    return
  }

  if (typeof timeout === 'function') {
    await timeout(speechBubble)
    remove()
    return
  }

  if (timeout instanceof Promise) {
    await timeout
    remove()
  }
}
