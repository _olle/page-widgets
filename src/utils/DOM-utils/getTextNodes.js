/* eslint-disable no-cond-assign */
export const getAllTextNodes = (el = document.body, filter) => {
    let n
    const a = []
    const walk = document.createTreeWalker(el, NodeFilter.SHOW_TEXT, filter)
    while(n = walk.nextNode()) {
        a.push(n)
    }
    return a
}

export const getTextNodes = (el = document.body, filter) => document.createTreeWalker(el, NodeFilter.SHOW_TEXT, filter)

export const hasText = (el) => /[a-zA-Z]/.test(el.textContent)

export const isHidden = (el) => {
    if (el.offsetParent === null) {
        return true
    }
    const style =  window.getComputedStyle(el)
    return style.display === 'none'
}
