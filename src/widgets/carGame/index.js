import p5 from '../../lib/p5'
import Matter from 'matter-js'
import { takeScreenshot, setupDebugImage, setupObstacleDrawing } from '../../screenshot'
import { getFuzzyForbiddenPoints } from '../../screenshot/imageUtils'
import carSkin from 'url:./car_green.png'
import { makeSpeechBubble } from '../../utils/speechBubble/speechBubble.js'

const sketch = (s) => {
  let engine
  let world
  let car
  let carPic
  let width
  let height
  let totalHeight
  let showDebugImageIfToggled = Function.prototype
  let showObstaclesIfToggled = Function.prototype
  let obstacles = []
  let rearTirePositionHistory = []
  const trailMaxAge = 10000

  const BASE_FRICTION = 0.9
  const BASE_FRICTION_AIR = 0.15
  
  // short-term solution to allow page backgound colors
  const allowedColors = [[255, 255, 255]]

  // preload the car image before the sketch starts
  s.preload = () => {
    carPic = s.loadImage(carSkin)
  }

  // As yet it takes a while to setup the game, so we reload the page when the window is resized
  s.windowResized = async () => {
    location.reload()
  }

  s.setup = async () => {
    s.pixelDensity(1)
    width = s.windowWidth
    height = s.windowHeight
    totalHeight = document.documentElement.offsetHeight

    const c = s.createCanvas(width, totalHeight)
    c.position(0, 0)

    // Take screenshot of the page to get forbidden points and add them as static obstacles
    // This needs to happen async to not block the setup
    takeScreenshot()
      .then(refImage => {
        const forbiddenPoints = getFuzzyForbiddenPoints(refImage.data, width, allowedColors, 10)
        const newObstacles = forbiddenPoints.map(([x, y]) => Matter.Bodies.rectangle(x, y, 1, 1, { isStatic: true }))
        obstacles = obstacles.concat(newObstacles)
        Matter.World.add(world, newObstacles)
        console.log('added', newObstacles.length, 'obstacles')
        showDebugImageIfToggled = setupDebugImage(s, refImage)
        showObstaclesIfToggled = setupObstacleDrawing(s, obstacles)
      })

    // setup the engine and world
    engine = Matter.Engine.create()
    world = engine.world
    engine.gravity.y = 0
    engine.gravity.x = 0
    
    // setup the car
    car = Matter.Bodies.rectangle(500, 200, 60, 30, {
      restitution: 0.02,
      frictionAir: BASE_FRICTION_AIR,
      friction: BASE_FRICTION,
      frictionStatic: 1.5,
      mass: 3 ,
      isStatic: false,
      label: 'car'
    })

    Matter.World.add(world, car)

    // restrict the car from leaving the screen
    // TODO: could perhaps be done with restrictions instead
    const wallTop = Matter.Bodies.rectangle(width / 2, -30 / 2, width, 30, { isStatic: true })
    const wallBottom = Matter.Bodies.rectangle(width / 2, height + 30 / 2, width, 30, { isStatic: true })
    const wallRight = Matter.Bodies.rectangle(width + 30 / 2, height / 2, 30, height, { isStatic: true })
    const wallLeft = Matter.Bodies.rectangle(-30 / 2, height / 2, 30, height, { isStatic: true })
    obstacles.push(wallTop, wallBottom, wallRight, wallLeft)
    Matter.World.add(world, [wallTop, wallBottom, wallRight, wallLeft])

    Matter.Runner.run(engine)
    preventDefaultControls()
  }

  // remember to draw the car AFTER the trails, otherwise the trails will be drawn on top of the car
  s.draw = () => {
    s.clear()
    handleInput(s)
    Matter.Engine.update(engine)
    showDebugImageIfToggled()
    showObstaclesIfToggled()
    drawTrails()
    drawCar()
  }

  // TODO: scaling to average free space
  function drawCar() {
    s.push()
    s.translate(car.position.x, car.position.y)
    s.rotate(car.angle)
    s.imageMode(s.CENTER)
    s.image(carPic, 0, 0, 60, 30)
    s.pop()
  }

  // draw a line through the history of the rear tires, fading to white the older the trail is
  function drawTrails() {
    const now = performance.now()
    rearTirePositionHistory = rearTirePositionHistory.filter(trail => trail.time > performance.now() - trailMaxAge)
    
    for (let i = 0; i < rearTirePositionHistory.length - 1; i++) {
      const currentTrail = rearTirePositionHistory[i]
      const nextTrail = rearTirePositionHistory[i+1]
      const dist = Matter.Vector.magnitude(
        Matter.Vector.sub(nextTrail.leftRearTire, currentTrail.leftRearTire)
      )
      const darkness = s.map(currentTrail.time, now, now - trailMaxAge, 76, 255)

      // Dont connect the trails if they are too far apart
      if (dist < 20) {
        s.strokeWeight(3)
        s.stroke(darkness)
        s.line(
          currentTrail.rightRearTire.x,
          currentTrail.rightRearTire.y,
          nextTrail.rightRearTire.x,
          nextTrail.rightRearTire.y
        )
        s.line(
          currentTrail.leftRearTire.x,
          currentTrail.leftRearTire.y,
          nextTrail.leftRearTire.x,
          nextTrail.leftRearTire.y
        )
      }
    }
  }

  // prevent the browser from scrolling around instead of the car moving
  function preventDefaultControls() {
    window.addEventListener('keydown', function (e) {
      if (['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight', ' ', 'Space'].indexOf(e.code) > -1) {
        e.preventDefault()
      }
    }, false)
  }

  function getRearAxlePosition() {
    const carLength = 60
    const rearOffset = carLength / 2
    const rearAxleX = car.position.x - Math.cos(car.angle) * rearOffset
    const rearAxleY = car.position.y - Math.sin(car.angle) * rearOffset
    return Matter.Vector.create(rearAxleX, rearAxleY)
  }

  function getRearTirePositions() {
    const rearAxle = getRearAxlePosition()
    const tireSpacing = 15

    const offsetX = Math.sin(car.angle) * (tireSpacing / 2)
    const offsetY = -Math.cos(car.angle) * (tireSpacing / 2)

    const leftRearTire = Matter.Vector.create(rearAxle.x - offsetX, rearAxle.y - offsetY)
    const rightRearTire = Matter.Vector.create(rearAxle.x + offsetX, rearAxle.y + offsetY)

    return { leftRearTire, rightRearTire }
  }

  function handleInput(s) {
    const baseAccelerationForce = 0.005
    const goForward = s.keyIsDown(s.UP_ARROW)
    const goBackward = s.keyIsDown(s.DOWN_ARROW)
    const turnLeft = s.keyIsDown(s.LEFT_ARROW)
    const turnRight = s.keyIsDown(s.RIGHT_ARROW)  
    const turbo = s.keyIsDown(32)

    function getTurnSpeed() {
      // Add a bit of randomness to the turn speed.
      // This makes making donuts and such a bit less stiff
      const rndFactor = Math.random() * 0.05
      if (goForward) { return car.speed * 0.01 + rndFactor } 
      if (goBackward) { return car.speed * -0.01 - rndFactor }
      return 0
    }

    if (goForward) {
      const forward = Matter.Vector.rotate({ x: baseAccelerationForce, y: 0 }, car.angle)
      Matter.Body.applyForce(car, car.position, forward)
    } 
    
    else if (goBackward) {
      const backward = Matter.Vector.rotate({ x: - baseAccelerationForce * 0.6 , y: 0 }, car.angle)
      Matter.Body.applyForce(car, car.position, backward)
    }

    if ((goForward || goBackward) && turnLeft) {
      Matter.Body.setAngle(car, car.angle - getTurnSpeed())
    }
    
    if ((goForward || goBackward) && turnRight) {
      Matter.Body.setAngle(car, car.angle + getTurnSpeed())
    }

    if (turbo) {
      const direction = goForward ? 1 : goBackward ? -1 : 0
      const forward = Matter.Vector.rotate({ x: baseAccelerationForce * 1.2 * direction, y: 0 }, car.angle)
      Matter.Body.applyForce(car, car.position, forward)
      Matter.Body.set(car, { friction: 0.8, frictionAir: 0.09 })
      
      if (turnLeft || turnRight) {
        rearTirePositionHistory.push({ ...getRearTirePositions(), time: performance.now() })
      } 
    } else {
      Matter.Body.set(car, { friction: BASE_FRICTION, frictionAir: BASE_FRICTION_AIR })
    }

    return {
      goForward,
      goBackward,
      turnLeft,
      turnRight,
      turbo,
    }
  }
}

const standalone = () => new p5(sketch)
const forOlleJohannessonDotCom = async () => {
  new p5(sketch)

  const keys = document.createElement('div')
  '↑←↓→ '.split('').map((arrow, index) => {
    const kbd = document.createElement('kbd')
    kbd.appendChild(document.createTextNode(arrow))
    keys.appendChild(kbd)
    kbd.style.cssText = `
      background-color: #eee;
      border-radius: 3px;
      border: 1px solid #b4b4b4;
      box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
      color: #333;
      display: inline-block;
      font-size: .85em;
      font-weight: 700;
      line-height: 1;
      white-space: nowrap;
      display: flex;
      justify-content: center;
      align-items: center;
      grid-area: key-${index}
    `
  })
  keys.style.cssText = `
    aspect-ratio: 1/1;
    width: 35%;
    display: grid;
    grid-template-columns: 33% 33% 33%;
    grid-template-rows: auto;
    grid-template-areas:
      ". key-0 ."
      "key-1 key-2 key-3"
      "key-4 key-4 key-4";
    column-gap: 5%;
    row-gap: 5%;
  `

  setTimeout( () => makeSpeechBubble(keys, 5000), 3000)
}

const canRun = bowser => bowser.isPlatform('desktop')

export { forOlleJohannessonDotCom, standalone, canRun }
