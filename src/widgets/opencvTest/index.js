import p5 from 'p5'
import Matter from 'matter-js'
import { takeScreenshot } from '../../screenshot'
import carSkin from 'url:./car_green.png'
import decomp from 'poly-decomp'
import { toMatterJsBody } from '../../utils/MatterJs'
import { getAverageLineHeight } from '../../utils/DOM-utils/averageLineHeight.js'
import { add, distance, multiply, normalize, seek, setMagnitude, subtract } from '../../utils/steering'
import ghostSkin from 'url:./ghost_white_eyes.png'
import Worker from 'web-worker:./worker'
import { getNormallyDistributedNumber } from './rnd.js'
import { makeSpeechBubble } from '../../utils/speechBubble/speechBubble.js'

window.p5 = p5
const rTreeWorker = new Worker()

const sketch = (s) => {
  const MAX_CAR_WIDTH = 60
  const MIN_CAR_WIDTH = 30
  const BASE_FRICTION = 0.9
  const BASE_FRICTION_AIR = 0.15
  const GHOST_MAX_FORCE = 0.175
  const GHOST_MAX_SPEED = 3.75
  const ghost_left_eye_offset = { x: 0.45, y: 0.4 }
  const ghost_right_eye_offset = { x: 0.8, y: 0.4 }
  const ghost_eye_radius = 0.125
  const trailMaxAge = 10000

  let engine
  let car
  let ghost
  let carPic
  let width
  let height
  let debugImg
  let pathFromGhostToCar = []
  let carSize = { width: MAX_CAR_WIDTH, height: MAX_CAR_WIDTH / 2 }
  let rearTirePositionHistory = []
  let ghostSize = { width: MAX_CAR_WIDTH / 2, height: MAX_CAR_WIDTH * (131 / 96) / 2 }
  let ghostPic
  let ghostTint = 0
  let ghostEyesAreShut = false
  let drawDebugPath = false
  // window.debug = function () {
  //   drawDebugPath = true
  // }

  // A decent heuristic for how large things are on a webpage
  // TODO: use this to map to the car and ghost size when making a chrome extension
  const lineHeight = getAverageLineHeight()

  const carParams = {
    restitution: 0.02,
    frictionAir: BASE_FRICTION_AIR,
    friction: BASE_FRICTION,
    frictionStatic: 1.5,
    mass: 3,
    isStatic: false,
    label: 'car'
  }

  const ghostParams = {
    restitution: 0.99,
    frictionAir: BASE_FRICTION_AIR * 0.75,
    friction: BASE_FRICTION * 0.75,
    frictionStatic: 1,
    mass: 3,
    isStatic: false,
    label: 'ghost',
    isSensor: true
  }

  const initGhost = () => {
    rTreeWorker.postMessage({
      type: 'find place to spawn ghost', data: {
        width,
        height,
        bodyWidth: ghostSize.width,
        bodyHeight: ghostSize.height,
        padding: 5
      }
    })
  }

  const initCar = () => {
    rTreeWorker.postMessage({
      type: 'find place to spawn car', data: {
        width,
        height,
        bodyWidth: carSize.width,
        bodyHeight: carSize.height,
        padding: 5
      }
    })
  }

  const updateClosestPathBetweenGhostAndCar = () => {
    if (ghost) {
      const howFarInTheFutureToGuess = s.frameRate() / 2
      const guessedFutureCarPosition = add(car.position, multiply(car.velocity, howFarInTheFutureToGuess))

      rTreeWorker.postMessage({
        type: 'find path between ghost and car', data: {
          from: ghost.position,
          to: guessedFutureCarPosition,
          gridSize: ghostSize.width
        }
      })
    }
  }

  rTreeWorker.onmessage = (event) => {
    const { type, result } = event.data
    switch (type) {
      case 'initiated R-Tree with obstacle polygons': {
        const bodies = result.map(toMatterJsBody)
        Matter.World.add(engine.world, bodies)
        console.timeEnd('obstacle detection')
        console.log(`added ${bodies.length} obstacles`)

        initCar()
        setTimeout(initGhost, 10 * 1000)
        setInterval(updateClosestPathBetweenGhostAndCar, 500)
        s.loop()
        break
      }

      case 'found place to spawn car': {
        car = Matter.Bodies.rectangle(
          result.x,
          result.y,
          MAX_CAR_WIDTH,
          MAX_CAR_WIDTH / 2,
          carParams)
        Matter.World.add(engine.world, car)
        break
      }

      case 'found place to spawn ghost': {
        ghostTint = 0
        ghost = Matter.Bodies.rectangle(
          result.x,
          result.y,
          ghostSize.width,
          ghostSize.height,
          ghostParams)
        Matter.World.add(engine.world, ghost)
        break
      }

      case 'found path between ghost and car': {
        if (result.length) {
          pathFromGhostToCar = result
        }
        break
      }
    }
  }

  function ghostBlink() {
    ghostEyesAreShut = true
    setTimeout(() => {
      ghostEyesAreShut = false
    }, 100)

    setTimeout(ghostBlink, getNormallyDistributedNumber(5000, 1500))
  }

  // preload the car image before the sketch starts
  s.preload = () => {
    carPic = s.loadImage(carSkin)
    ghostPic = s.loadImage(ghostSkin)
  }

  // As yet it takes a while to setup the game, so we reload the page when the window is resized
  s.windowResized = async () => {
    location.reload()
  }

  s.setup = async () => {
    s.noLoop()
    s.frameRate(60)
    s.pixelDensity(1)
    width = s.windowWidth
    height = document.documentElement.offsetHeight

    const c = s.createCanvas(width, height)
    c.position(0, 0)

    // setup the engine and world
    window.decomp = decomp
    Matter.Common.setDecomp(decomp)
    engine = Matter.Engine.create()
    engine.gravity.y = 0
    engine.gravity.x = 0

    // take screenshot and initiate obstacles
    console.time('obstacle detection')
    takeScreenshot().then(imgData => rTreeWorker.postMessage({
      type: 'init',
      data: { imgData, width, height }
    }, [imgData.data.buffer]))

    ghostBlink()
    preventDefaultControls()
  }

  s.draw = () => {
    Matter.Engine.update(engine, 1000 / 60)
    s.clear()
    if (debugImg) {
      const img = s.createImage(debugImg.width, debugImg.height)
      img.loadPixels()
      img.pixels.set(debugImg.data)
      img.updatePixels()
      s.image(img, 0, 0)
    }

    if (drawDebugPath && pathFromGhostToCar.length) {
      s.fill(0, 255, 0)
      pathFromGhostToCar.forEach(v => s.circle(v.x, v.y, 10))

      s.noFill()
      s.stroke(0, 0, 255)
      s.beginShape()
      pathFromGhostToCar.forEach(v => s.vertex(v.x, v.y))
      s.endShape()
      s.noStroke()
    }

    handleInput(s)
    drawTrails()
    drawCar()
    drawGhost()
    steerGhost()

    if (ghost && car && Matter.Collision.collides(ghost, car)) {
      window.location.reload()
    }
  }

  function preventDefaultControls() {
    window.addEventListener('keydown', function (e) {
      if (['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight', ' ', 'Space'].indexOf(e.code) > -1) {
        e.preventDefault()
      }
    }, false)
  }

  function handleInput(s) {
    if (!car) {
      return
    }
    const baseAccelerationForce = 0.005
    const goForward = s.keyIsDown(s.UP_ARROW)
    const goBackward = s.keyIsDown(s.DOWN_ARROW)
    const turnLeft = s.keyIsDown(s.LEFT_ARROW)
    const turnRight = s.keyIsDown(s.RIGHT_ARROW)
    const turbo = s.keyIsDown(32)

    function getTurnSpeed() {
      // Add a bit of randomness to the turn speed.
      // This makes making donuts and such a bit less stiff
      const rndFactor = Math.random() * 0.05
      if (goForward) { return car.speed * 0.01 + rndFactor }
      if (goBackward) { return car.speed * -0.01 - rndFactor }
      return 0
    }

    if (goForward) {
      const forward = Matter.Vector.rotate({ x: baseAccelerationForce, y: 0 }, car.angle)
      Matter.Body.applyForce(car, car.position, forward)
    }

    else if (goBackward) {
      const backward = Matter.Vector.rotate({ x: - baseAccelerationForce * 0.6 , y: 0 }, car.angle)
      Matter.Body.applyForce(car, car.position, backward)
    }

    if ((goForward || goBackward) && turnLeft) {
      Matter.Body.setAngle(car, car.angle - getTurnSpeed())
    }

    if ((goForward || goBackward) && turnRight) {
      Matter.Body.setAngle(car, car.angle + getTurnSpeed())
    }

    if (turbo) {
      const direction = goForward ? 1 : goBackward ? -1 : 0
      const forward = Matter.Vector.rotate({ x: baseAccelerationForce * 1.2 * direction, y: 0 }, car.angle)
      Matter.Body.applyForce(car, car.position, forward)
      Matter.Body.set(car, { friction: 0.8, frictionAir: 0.09 })

      if (turnLeft || turnRight) {
        rearTirePositionHistory.push({ ...getRearTirePositions(), time: performance.now() })
      }
    } else {
      Matter.Body.set(car, { friction: BASE_FRICTION, frictionAir: BASE_FRICTION_AIR })
    }

    return {
      goForward,
      goBackward,
      turnLeft,
      turnRight,
      turbo,
    }
  }

  function drawCar() {
    if (car) {
      s.push()
      s.translate(car.position.x, car.position.y)
      s.rotate(car.angle)
      s.imageMode(s.CENTER)
      s.image(carPic, 0, 0, carSize.width, carSize.height)
      s.pop()
    }
  }

  function getRearAxlePosition() {
    const carLength = 60
    const rearOffset = carLength / 2
    const rearAxleX = car.position.x - Math.cos(car.angle) * rearOffset
    const rearAxleY = car.position.y - Math.sin(car.angle) * rearOffset
    return Matter.Vector.create(rearAxleX, rearAxleY)
  }

  function getRearTirePositions() {
    const rearAxle = getRearAxlePosition()
    const tireSpacing = 15

    const offsetX = Math.sin(car.angle) * (tireSpacing / 2)
    const offsetY = -Math.cos(car.angle) * (tireSpacing / 2)

    const leftRearTire = Matter.Vector.create(rearAxle.x - offsetX, rearAxle.y - offsetY)
    const rightRearTire = Matter.Vector.create(rearAxle.x + offsetX, rearAxle.y + offsetY)

    return { leftRearTire, rightRearTire }
  }

  function drawTrails() {
    const now = performance.now()
    rearTirePositionHistory = rearTirePositionHistory.filter(trail => trail.time > performance.now() - trailMaxAge)

    for (let i = 0; i < rearTirePositionHistory.length - 1; i++) {
      const currentTrail = rearTirePositionHistory[i]
      const nextTrail = rearTirePositionHistory[i+1]
      const dist = Matter.Vector.magnitude(
        Matter.Vector.sub(nextTrail.leftRearTire, currentTrail.leftRearTire)
      )
      const darkness = s.map(currentTrail.time, now, now - trailMaxAge, 76, 255)

      // Dont connect the trails if they are too far apart
      if (dist < 20) {
        s.strokeWeight(3)
        s.stroke(darkness)
        s.line(
          currentTrail.rightRearTire.x,
          currentTrail.rightRearTire.y,
          nextTrail.rightRearTire.x,
          nextTrail.rightRearTire.y
        )
        s.line(
          currentTrail.leftRearTire.x,
          currentTrail.leftRearTire.y,
          nextTrail.leftRearTire.x,
          nextTrail.leftRearTire.y
        )
      }
    }
  }

  function drawGhost() {
    if (ghost) {
      s.push()
      s.translate(ghost.position.x, ghost.position.y)
      s.imageMode(s.CENTER)
      if (ghostTint < 255) {
        s.tint(255, ghostTint)
        ghostTint++
      }
      s.image(ghostPic, 0, 0, ghostSize.width, ghostSize.height)
      s.pop()

      s.push()
      s.translate(ghost.position.x - ghostSize.width / 2, ghost.position.y - ghostSize.height / 2)
      const leftEyeCenter = {
        x: ghostSize.width * ghost_left_eye_offset.x,
        y: ghostSize.height * ghost_left_eye_offset.y
      }
      const rightEyeCenter = {
        x: ghostSize.width * ghost_right_eye_offset.x,
        y: ghostSize.height * ghost_right_eye_offset.y
      }

      s.strokeWeight(1)
      s.stroke(0)
      s.fill(0)
      if (ghostEyesAreShut) {
        const eyelidSize = ghostSize.width * ghost_eye_radius * 2
        s.ellipse(leftEyeCenter.x, leftEyeCenter.y, eyelidSize)
        s.ellipse(rightEyeCenter.x, rightEyeCenter.y, eyelidSize)
      } else {
        const direction = normalize(subtract(car.position, ghost.position))
        const positioned = setMagnitude(direction, ghostSize.width * ghost_eye_radius / 2)
        const leftEyePosition = add(leftEyeCenter, positioned)
        const rightEyePosition = add(rightEyeCenter, positioned)
        s.ellipse(leftEyePosition.x, leftEyePosition.y, 5)
        s.ellipse(rightEyePosition.x, rightEyePosition.y, 5)
      }
      s.noFill()
      s.noStroke()
      s.pop()
    }
  }

  function steerGhost() {
    if (!(ghost && car && pathFromGhostToCar.length)) { return }
    const velocity = Matter.Body.getVelocity(ghost)
    const nextSegment = pathFromGhostToCar.filter(p => distance(ghost.position, p) > ghostSize.height)[0]
    const position = ghost.position
    const steeringForce = seek(position, nextSegment, velocity, GHOST_MAX_SPEED, GHOST_MAX_FORCE)

    Matter.Body.applyForce(ghost, ghost.position, multiply(steeringForce, 0.005))
  }
}

const standalone = () => new p5(sketch)
const forOlleJohannessonDotCom = async () => {
  new p5(sketch)

  const keys = document.createElement('div')
  '↑←↓→ '.split('').map((arrow, index) => {
    const kbd = document.createElement('kbd')
    kbd.appendChild(document.createTextNode(arrow))
    keys.appendChild(kbd)
    kbd.style.cssText = `
      background-color: #eee;
      border-radius: 3px;
      border: 1px solid #b4b4b4;
      box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
      color: #333;
      display: inline-block;
      font-size: .85em;
      font-weight: 700;
      line-height: 1;
      white-space: nowrap;
      display: flex;
      justify-content: center;
      align-items: center;
      grid-area: key-${index}
    `
  })
  keys.style.cssText = `
    aspect-ratio: 1/1;
    width: 35%;
    display: grid;
    grid-template-columns: 33% 33% 33%;
    grid-template-rows: auto;
    grid-template-areas:
      ". key-0 ."
      "key-1 key-2 key-3"
      "key-4 key-4 key-4";
    column-gap: 5%;
    row-gap: 5%;
  `

  setTimeout( () => makeSpeechBubble(keys, 5000), 3000)
}

const canRun = bowser => bowser.isPlatform('desktop')

export { forOlleJohannessonDotCom, standalone, canRun }
