export const getProfilePic = () => document.querySelector('#profile-picture')

const orig = getProfilePic().getAttribute('src')

export const changeTo = (newImgUrl) => getProfilePic().src = newImgUrl

export const restore = () => changeTo(orig)
