import RBush from 'rbush'
import { isNotImagePerimeter, toClipperJsShape, toJSON } from '../MatterJs/index.js'

/**
 * The current Idea is to use an R-Tree to store the polygons, mainly as it makes
 * it very fast to find overlapping polygons when initalizing the map.
 * Actual collision detection can be managed by Matter-Js which uses a
 * broad-band collision detection algorithm. So if stuff moves around we don't
 * have to update the R-Tree.
 *
 * I know it is a bit wierd to have a global variable here, but it's simple development
 * for trying things out, and if I go ahead and put this in a Worker Thread, then
 * I won't have to pass the tree (or its ID) around all the time.
 *
 * Really, how many R-Trees do I need?
 */
let rtree

/**
 *
 * @param {Array[{x: number, y: number}]} contours
 * @param {number} width
 * @param {number} height
 */
export function initRTree(contours, width, height) {
  const shapes = contours
    .map(toClipperJsShape)
    .filter(shape => isNotImagePerimeter(shape, width, height))

  rtree = new RBush()
  rtree.load(shapes.map(shape => ({ shape, ...boundingBoxToSearchArea(shape.shapeBounds()) })))
}

/**
 * Filter out shapes that are contained in other shapes
 * WARNING: this is a destructive operation, it will modify the original tree
 *
 * Shapes here refer to this data type:
 * https://github.com/Doodle3D/clipper-js?tab=readme-ov-file#readme
 *
 * @returns {Array<Shape>}
 */
export function filterOutShapesContainedInOtherShapes() {
  const shapes = rtree.all().map(data => data.shape)
  const toRemove = new Set()

  shapes
    // sort smallest first
    .toSorted((a, b) => Math.abs(a.totalArea()) - Math.abs(b.totalArea()))
    .forEach(shape => {
      if (toRemove.has(shape)) {
        return
      }
      rtree.search(boundingBoxToSearchArea(shape.shapeBounds()))
        .filter(neighbor => neighbor.shape !== shape && !toRemove.has(neighbor.shape))
        .forEach(neighbor => {
          if (isShapeContainedInOtherShape(shape, neighbor.shape)) {
            toRemove.add(neighbor)
          }
        })
    })

  // TODO: does it really delete them, or do i need to include its bounding box?
  toRemove.forEach(shape => rtree.remove(shape))
  return shapes.filter(shape => !toRemove.has(shape)).map(toJSON)
}

// NOTE: only works if shape is simple (no holes)
export function isShapeContainedInOtherShape(outerShape, innerShape) {
  return innerShape.paths[0].every(point => outerShape.pointInShape(point))
}

export function boundingBoxToSearchArea(boundingBox) {
  return {
    minX: boundingBox.left,
    minY: boundingBox.top,
    maxX: boundingBox.right,
    maxY: boundingBox.bottom
  }
}

export function getValidSpawnPointRTree(mapWidth, mapHeight, bodyWidth, bodyHeight, padding = 5) {
  let attempts = 1000
  while (attempts--) {
    // Generate a random position within the map bounds
    const spawnX = Math.floor(Math.random() * (mapWidth - bodyWidth - padding * 2) + padding)
    const spawnY = Math.floor(Math.random() * (mapHeight - bodyHeight - padding * 2) + padding)
    const spawnBox = { minX: spawnX, minY: spawnY, maxX: spawnX + bodyWidth, maxY: spawnY + bodyHeight }
    const collisions = rtree.search(spawnBox)

    if (collisions.length === 0) {
      return { x: spawnX, y: spawnY }
    }
  }

  console.error('⚠ Failed to find a valid spawn point!')
  return null
}

// /**
//    * Convert a contour to a clipperjs shape
//    * @param {Array<{x: number, y: number}> } contour
//    * @returns {Shape}
//    */
// const toClipperJsShape = contour => new Shape([contour], true, true, true, true)
// .clean(0.1)
// .fixOrientation()

export function aStar(start, goal, gridSize) {
  const openSet = [start]
  const cameFrom = new Map()
  const gScore = new Map()
  const fScore = new Map()
  gScore.set(start, 0)
  fScore.set(start, euclidianDistance(start, goal))

  let i = 0
  while (openSet.length > 0) {
    if(i++ > 10000) {
      break;
    }

    const current = openSet
      .sort((a, b) => (fScore.get(a) ?? Number.MAX_SAFE_INTEGER) - (fScore.get(b) ?? Number.MAX_SAFE_INTEGER))
      .shift()

    const found = current.x <= goal.x &&
                  current.y <= goal.y &&
                  current.x + gridSize >= goal.x &&
                  current.y + gridSize >= goal.y

    if (found) {
      return reconstructPath(cameFrom, current)
    }

    const currentGScore = gScore.get(current) ?? Number.MAX_SAFE_INTEGER

    getNeighborGridCells(current, gridSize)
      .filter(n => !hasObstacle(n, gridSize))
      .forEach(neighbour => {
        const tentativeGScore = currentGScore + 1
        if (tentativeGScore < (gScore.get(neighbour) ?? Number.MAX_SAFE_INTEGER)) {
          cameFrom.set(neighbour, current)
          gScore.set(neighbour, tentativeGScore)
          fScore.set(neighbour, tentativeGScore + euclidianDistance(neighbour, goal))
          if (!openSet.some(node => node.x === neighbour.x && node.y === neighbour.y)) {
            openSet.push(neighbour)
          }
        }
    })
  }

  return []
}

const reconstructPath = (cameFrom, current) => {
  const path = []
  while (cameFrom.has(current)) {
    path.push(current)
    current = cameFrom.get(current)
  }
  return path.reverse()
}

function euclidianDistance(a, b) {
  return Math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2)
}

export function getFourNeighbors(node, gridSize) {
  const origin = centered
    ? { x: node.x + gridSize / 2, y: node.y + gridSize / 2 }
    : node

  return [
      { x: origin.x + gridSize, y: origin.y }, // Right
      { x: origin.x - gridSize, y: origin.y }, // Left
      { x: origin.x, y: origin.y + gridSize }, // Down
      { x: origin.x, y: origin.y - gridSize }  // Up
  ]
}

export function getNeighborGridCells(node, gridSize, centered = false) {
  const origin = centered
    ? { x: node.x + gridSize / 2, y: node.y + gridSize / 2 }
    : node

  return [
      { x: origin.x + gridSize, y: origin.y }, // Right
      { x: origin.x - gridSize, y: origin.y }, // Left
      { x: origin.x, y: origin.y + gridSize }, // Down
      { x: origin.x, y: origin.y - gridSize }, // Up
      { x: origin.x + gridSize, y: origin.y + gridSize }, // Down-Right
      { x: origin.x - gridSize, y: origin.y + gridSize }, // Down-Left
      { x: origin.x + gridSize, y: origin.y - gridSize }, // Up-Right
      { x: origin.x - gridSize, y: origin.y - gridSize }  // Up-Left
  ]
}

function hasObstacle(node, gridSize) {
  const searchArea = {
      minX: node.x - gridSize / 2,
      minY: node.y - gridSize / 2,
      maxX: node.x + gridSize / 2,
      maxY: node.y + gridSize / 2
  }

  return rtree.search(searchArea).length > 0
}

export function findObstacles(searchArea) {
  const area = searchArea.x1 && searchArea.y1 && searchArea.x2 && searchArea.y2 ? {
      minX: searchArea.x1,
      minY: searchArea.y1,
      maxX: searchArea.x2,
      maxY: searchArea.y2
  } : searchArea

  return rtree.search(area) ?? []
}

export function findObstacleVertices(searchArea) {
  return findObstacles(searchArea)
    .flatMap(obstacle => obstacle.shape.paths[0]
    .flatMap(point => ({ x: point.X, y: point.Y })))
}

