import globals from 'globals'
import pluginJs from '@eslint/js'


/** @type {import('eslint').Linter.Config[]} */
export default [
  { languageOptions: { globals: globals.browser } },
  pluginJs.configs.recommended,
  {
    rules: {
      'semi': ['error', 'never'],
      'quotes': ['error', 'single', { 'allowTemplateLiterals': true }],
      'prefer-const': 'warn',
      'object-curly-spacing': ['error', 'always']
    }
  },
  {
    'env': {
    'jest': true
  },
  'plugins': ['jest'],
  'extends': ['plugin:jest/recommended']
  }
]