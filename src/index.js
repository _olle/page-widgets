import Bowser from 'bowser'

const bowser = Bowser.getParser(window.navigator.userAgent)


let modules

if (sessionStorage.getItem('modules')) {
    modules = JSON.parse(sessionStorage.getItem('modules'))
    console.debug('loaded modules from sessionStorage', modules)
} else {
    console.debug('no modules in sessionStorage, generating new ones')
    modules = [1,2,3,4,5,6].sort(() => Math.random() - 0.5)
    sessionStorage.setItem('modules', JSON.stringify(modules))
}

// TODO: mobile controls
const choose = async (round = 0) => {
    if (round > 1000) {
        return new Promise.resolve({
            forOlleJohannessonDotCom: Function.prototype,
            standalone: Function.prototype,
            canRun: () => false
        })
    }
    // This is a simple way to just run a specific module while developing:
    // const pongModule = await import('./widgets/pong')
    // console.debug('loading pong module...')
    // return pongModule

    let nextModule = 0

    if (sessionStorage.getItem('nextModule')) {
        nextModule = parseInt(sessionStorage.getItem('nextModule'))
        console.debug('loaded nextModule from sessionStorage', nextModule)
    } else {
        nextModule = 0
        sessionStorage.setItem('nextModule', nextModule)
    }

    sessionStorage.setItem('nextModule', (nextModule + 1) % modules.length)

    const chooseSomethingElse  = () => choose(round + 1)

    switch (nextModule) {
        case 0: {
            const japaneseModule = await import('./widgets/japanese')
            console.debug('loading japanese module...')
            return (await japaneseModule.canRun(bowser))
                ? japaneseModule
                : chooseSomethingElse()
          }

        case 1: {
            const rapModule = await import('./widgets/rap')
            console.debug('loading rap module...')
            return (await rapModule.canRun(bowser))
              ? rapModule
              : chooseSomethingElse()
        }

        case 2: {
            const karaokeModule = await import('./widgets/karaoke')
            console.debug('loading karaoke module...')
            return (await karaokeModule.canRun(bowser))
                ? karaokeModule
                : chooseSomethingElse()
        }

        case 3: {
            const seaModule = await import('./widgets/sea')
            console.debug('loading sea module...')
            return (await seaModule.canRun(bowser))
                ? seaModule
                : chooseSomethingElse()
        }

        case 4: {
            const pongModule = await import('./widgets/pong')
            console.debug('loading pong module...')
            return (await pongModule.canRun(bowser))
                ? pongModule
                : chooseSomethingElse()
        }

        default: {
            const carModule = await import('./widgets/carGame')
            console.debug('loading car module...')
            return (await carModule.canRun(bowser))
                ? carModule
                : chooseSomethingElse()
        }
    }
}

export default () => choose()
