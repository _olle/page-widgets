// adapted from p5.play example
// https://molleindustria.github.io/p5.play/examples/index.html?fileName=pong.js

import p5 from 'p5'
import { takeScreenshot } from '../../screenshot'
import { getContours, waitForOpenCV } from '../../utils/opencv/index.js'
import { filterOutShapesContainedInOtherShapes, initRTree } from '../../utils/R-tree/index.js'
import { toMatterJsBody } from '../../utils/MatterJs/index.js'
import Matter from 'matter-js'
import decomp from 'poly-decomp'
import { setMagnitude } from '../../utils/steering/index.js'

// The workflow is like this:
// Initialise all constants and variables. Detect obstacles from the webpage content
// and turn them into interactable matter.js bodies. Then the game can start. All
// the physics and running of the game is really handled by matter.js, so there
// is hardly any code interacting with the gameplay except setting up the bodies,
// moving the paddles vertically and shooting off the ball in some direction.
const sketch = (s) => {

  // Be nice and credit the idea source
  console.group('credits')
  console.log('adapted from a p5.play example')
  console.log('https://molleindustria.github.io/p5.play/examples/index.html?fileName=pong.js')
  console.groupEnd()

  // These are constants not changing during either
  // setup or the game
  const MAX_SPEED = 0.1
  const BALL_SIZE = 10
  const PADDLE_WIDTH = 10
  const PADDLE_INSET = 30
  const black70 = s.color(56,56,56)

  // These are variables that need to be defined later on
  let engine
  let paddleA, paddleB, ball
  let width, height, totalHeight
  let paddleHeight

  // The constants for the ball. I think the restiution gets
  // overridden by the isStatic property, but I still
  // include it.
  // No friction or anything, this is practically air hockey
  // The mass is a magic number, and really doesn't matter
  // much since everything else is static and we're controlling
  // the velocity hard anyway
  const ballParams = {
    restitution: 1,
    frictionAir: 0,
    friction: 0,
    frictionStatic: 0,
    mass: 3,
    isStatic: false,
    label: 'ball',
  }

  // The obstacles are all static, they don't really do anything
  const obstacleParams = {
    isStatic: true,
    restitution: 1
  }

  /**
   * Returns an object with a random position and direction at full speed.
   * The Y direction is kept a little modest, so the game doesn't turn
   * into watching a ball bounce up and down without player interaction.
   *
   * @returns {{position: {x: number, y: *}, velocity: {x: number, y: number}}}
   */
  const ballRndInit = () => ({
    position: { x: width / 2, y: s.random(height) },
    velocity: {
      x: Math.sign(Math.random() - 0.5) * MAX_SPEED,
      y: Math.sign(Math.random() - 0.5) * 0.05
    }
  })

  /**
   * Give the ball a random position and direction at full speed
   * Called when the game starts or restarts (as when the ball
   * goes out the sides)
   *
   * @returns {{position: {x: number, y: *}, velocity: {x: number, y: number}}}
   */
  const restartBall = () => {
    const init = ballRndInit();
    Matter.Body.setPosition(ball, init.position)
    Matter.Body.setVelocity(ball, init.velocity)
    Matter.Body.applyForce(ball, ball.position, init.velocity)
    return init
  }

  s.setup = async () => {
    // No need to loop before we have initialized everything
    s.noLoop()

    // Set the framerate to a constant (as much as possible) to simplify
    // sync with matter.js
    s.frameRate(60)

    // This can be important for screenshotting and syncing pixelwise
    // with the canvas and image size. Some screens have high resolutions,
    // which might cause problems with lining things up correctly.
    s.pixelDensity(1)

    // Initialise context / system dependant variables
    width = s.windowWidth
    totalHeight = document.documentElement.offsetHeight
    height = s.windowHeight
    paddleHeight = height / 6

    // Initialise Matter.js.
    // - Decomp is important for making the bodies out of vertices.
    // - Engine instance is created
    // - Gravity is not needed
    window.decomp = decomp
    Matter.Common.setDecomp(decomp)
    engine = Matter.Engine.create()
    engine.gravity.y = 0
    engine.gravity.x = 0

    // Initialize P5
    // The stickyness is a leftover from when I had a very tall page
    // that scrolled for a while. Then I made the performance optimization
    // to keep the canvas to screen height and have scroll along. All Y
    // coordinates had to take the scrolling into consideration. I leave it
    // here in case I need it for later.
    const c = s.createCanvas(width, totalHeight)
    c.position(0, 0)
    document.getElementsByClassName('p5Canvas')[0].style.position = 'fixed'

    // The money shot.
    // First, take a screenshot of the page. The image data is used by an opencv function
    // that finds contours in the image and returns polygon vertices. These need to be
    // cleaned up a little bit, perhaps simplified, and put into clockwise order (per polygon)
    // for matter.js to handle them. It is standard practice for dealing with polygons, it seems.
    // I use the R-Tree to spacially locate holes and remove them as I don't need them
    // (this step might be able to be improved). Then Matter.js can make them into bodies
    // and these can now be interacted with. The polygons are not pixel perfect, but as long
    // as I don't have to draw them on screen it is totally fine.
    waitForOpenCV()
      .then(() => (console.time('obstacle detection')))
      .then(takeScreenshot)
      .then(getContours)
      .then(contours => initRTree(contours, width, totalHeight))
      .then(() => filterOutShapesContainedInOtherShapes())
      .then(shapes => shapes.map(toMatterJsBody))
      .then(bodies => {
        Matter.World.add(engine.world, bodies)
        bodies.forEach(body => Matter.Body.set(body, 'restitution', 1))
        console.timeEnd('obstacle detection')
        console.log(`added ${bodies.length} obstacles`)
        s.loop()
      })

    // Initialise the ball, paddles, floor and ceiling.
    const wallTop = Matter.Bodies.rectangle(
      width / 2,
      -30 / 2,
      width,
      30,
      obstacleParams)

    const wallBottom = Matter.Bodies.rectangle(
      width / 2,
      height + 30 / 2,
      width,
      30,
      obstacleParams)

    paddleA = Matter.Bodies.rectangle(
      PADDLE_INSET,
      height / 2,
      PADDLE_WIDTH,
      paddleHeight,
      obstacleParams)

    paddleB = Matter.Bodies.rectangle(
      width - PADDLE_INSET,
      height / 2,
      PADDLE_WIDTH,
      paddleHeight,
      obstacleParams)

    // The ball should really be a circle. The angles resulting from bouncing
    // an actual square around is quite dissapointing.
    // I still draw it as a square, though, to keep in style with the game.
    ball = Matter.Bodies.circle(
      0,
      0,
      BALL_SIZE,
      ballParams)

    // Add them to the matter.js world
    Matter.World.add(engine.world, [wallTop, wallBottom, paddleA, paddleB, ball])

    // for some reason, setting { isStatic: true } overrides some settings, so we have
    // to manually set the restitution
    ;[wallTop, wallBottom, paddleA, paddleB, ball].forEach(body => {
      Matter.Body.set(body, 'restitution', 1)
    })

    // Initalize the ball position, direction and start the game.
    restartBall()

    // Restitution alone seems not to do the trick of letting the ball never lose its energy.
    // This is a bit ugly, but it does the trick: Reset the ball velocity after every
    // collision.
    Matter.Events.on(engine, "collisionEnd", () => {
      Matter.Body.setVelocity(ball, setMagnitude(ball.velocity, MAX_SPEED * 100))
    })
  }

  s.draw = () => {
    // When combining Matter.js and P5 it is best to update the enging manually in the draw()
    // loop, since Matter.js has its own concept of time (as in seconds for velocity, seconds
    // squared for acceleration and so on). This might not sync up as expected with the
    // tick of the draw loop, so here we manually tick the matter.js world along with the
    // same framerate as the draw() loop.
    Matter.Engine.update(engine, 1000 / 60)

    // Clear out stuff from the last frame
    s.clear()

    // Constrain the paddles Y positions to the top of the window (including their height)
    // and move them vertically along with the mouse
    const paddleYPosition = s.constrain(s.mouseY, paddleHeight / 2, height - paddleHeight / 2)
    Matter.Body.setPosition(paddleA, { x: 30, y: paddleYPosition })
    Matter.Body.setPosition(paddleB, { x: width - 30, y: paddleYPosition })

    // If the ball leaves the screen to the left or the right the game restarts.
    if (ball.position.x < 0 || ball.position.x > width) {
      restartBall()
    }

    // Drawing the paddles and the ball
    if (ball && paddleA && paddleB) {
      s.rectMode(s.CENTER)
      s.stroke(0)
      s.fill(black70)
      s.rect(paddleA.position.x, paddleA.position.y, 10, height / 6)
      s.rect(paddleB.position.x, paddleB.position.y, 10, height / 6)
      s.rect(ball.position.x, ball.position.y, BALL_SIZE, BALL_SIZE)
    }
  }
}

const run = () => new p5(sketch)

const forOlleJohannessonDotCom = run
const standalone = run
const canRun = (bowser) => bowser.isPlatform('desktop')

export { forOlleJohannessonDotCom, standalone, canRun }
