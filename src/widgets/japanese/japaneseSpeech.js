let audioContext
let mainGain
let audioSource

const init = async () => {
    if (!audioContext) {
        audioContext = new AudioContext()
        mainGain = audioContext.createGain()
        mainGain.gain.setValueAtTime(1, 0)
        mainGain.connect(audioContext.destination)
    }
}

const withAudio = fn => (...args) => {
    init().then(() => fn(...args))
}

export const play = withAudio(async (soundArrayBuffer, onEnded) => {
    const audioBuffer = await audioContext.decodeAudioData(soundArrayBuffer)
    audioSource = audioContext.createBufferSource(audioBuffer)
    audioSource.buffer = audioBuffer
    audioSource.connect(mainGain)
    audioSource.loop = false
    audioSource.onended = onEnded
    audioSource.start()
})

export const stop = withAudio(() => {
    audioSource?.stop()
})
