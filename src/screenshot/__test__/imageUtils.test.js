import fs from 'fs'
import path from 'path'
import * as PImage from 'pureimage'
import {
    decodeRunLengthImage,
    makeBoolImage,
    makeIntegralImage,
    rectContainsForbiddenColor,
    runLengthEncodeImage, sliceRunLengthImage,
    getForbiddenPoints, getColFromImgData, getFuzzyForbiddenPoints
} from '../imageUtils.js'
import { arrayEquals } from '../../utils/arrayEquals.js'
import { compareArrayWithTolerance } from '../../utils/fuzzy.js'

describe('imageUtils', () => {
    const white = [255, 255, 255]
    const black = [0, 0, 0]
    const red = [255, 0 , 0]
    const green = [0, 255, 0]
    const blue = [0, 0, 255]
    let file, img, ctx, testImgData

    beforeEach(async () => {
        file = fs.readFileSync(path.resolve() + '/src/screenshot/__test__/test.png')
        img = await PImage.decodePNGFromStream(fs.createReadStream(path.resolve() + '/src/screenshot/__test__/test.png'))
        ctx = img.getContext('2d')
        ctx.drawImage(img, 0, 0, img.width / 4, img.height / 4)
        testImgData = ctx.getImageData(0, 0, 25, 25)
    })

    describe('makeBoolImage', () => {
        it('resulting typed array contains only 0 and 1', () => {
            const bool = makeBoolImage(testImgData.data, [white])
            const zerosAndOnes = bool.filter(n => n === 0 || n === 1)
            expect(zerosAndOnes.length).toEqual(bool.length)
        })

        it('is exactly a quarter of the length of the original imagedata array', () => {
            const bool = makeBoolImage(testImgData.data, [white])
            expect(bool.length).toEqual(testImgData.data.length / 4)
        })

        it('allowed color is 0', () => {
            const onlyWhite = makeBoolImage(testImgData.data, [white])
            const firstPixelIsWhite = testImgData.data.slice(0, 3).every((c, i) => c === white[i])
            expect(firstPixelIsWhite).toBeTruthy()
            expect(onlyWhite[0]).toBe(0)
        })

        it('disallowed color is 1', () => {
            const onlyBlack = makeBoolImage(testImgData.data, [black])
            const firstPixelIsWhite = testImgData.data.slice(0, 3).every((c, i) => c === white[i])
            expect(firstPixelIsWhite).toBeTruthy()
            expect(onlyBlack[0]).toBe(1)
        })

        it.skip('can allow colors', () => {
            const onlyWhite = makeBoolImage(testImgData.data, [white])
            const whiteAndBlack = makeBoolImage(testImgData.data, [white, black])
            const firstBlackPixelIndex = 25 * 5 + 6
            expect(onlyWhite[firstBlackPixelIndex]).toBe(1)
            expect(whiteAndBlack[firstBlackPixelIndex]).toBe(0)
        })
    })

    describe('integral image', () => {
        const ones = [
            [1, 1, 1],
            [1, 1, 1],
            [1, 1, 1]
        ].flat()

        const identity = [
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ].flat()

        describe('makeIntegralImage', () => {

            it('returns a Uint32Array', () => {
                const result = makeIntegralImage(ones, 3, 3)
                expect(result).toBeInstanceOf(Uint32Array)
            })

            it('constructs a flattend matrix with top and left padded with zeros', () => {
                const result = makeIntegralImage(ones, 3, 3)
                const topRow = result.slice(0, 4)
                const leftCol = result.filter((e, i) => i % 4 === 0)
                expect(result.length).toEqual(ones.length + 3 + 3 + 1)
                expect(topRow.every(n => n === 0)).toBeTruthy()
                expect(leftCol.every(n => n === 0)).toBeTruthy()
                expect(leftCol).toHaveLength(4)
            })

            it('every member is the sum of itself and the pixels to its left and/or above', () => {
                const expected = [
                    [0, 0, 0, 0],
                    [0, 1, 2, 3],
                    [0, 2, 4, 6],
                    [0, 3, 6, 9]
                ].flat()
                const result = makeIntegralImage(ones, 3, 3)
                const allMatches = result.every((e, i) => e === expected[i])
                expect(allMatches).toBeTruthy()
            })
        })

        describe('rectContainsForbiddenColor', () => {
            it('returns TRUE if area contains anything > 0', () => {
                const integral = makeIntegralImage(identity, 4, 4)
                const result = rectContainsForbiddenColor(integral, 0, 0, 2, 2, 4)
                expect(result).toBeTruthy()
            })

            it('returns FALSE if area contains nothing > 0', () => {
                const integral = makeIntegralImage(identity, 4, 4)
                const result = rectContainsForbiddenColor(integral, 2, 1, 3, 1, 4)
                expect(result).toBeFalsy()
            })
        })
    })

    describe('run-length encoding', () => {
        const original = [1, 1, 1, 2, 2, 3, 4, 4, 4, 4, 4]
        const encoding = [3, 1, 2, 2, 1, 3, 5, 4]

        it('runLengthEncodeImage run-lengths encodes array', () => {
            const result = runLengthEncodeImage(original)
            const isAsExpected = result.every((e, i) => e === encoding[i])
            expect(isAsExpected).toBeTruthy()
        })

        it('decodeRunLengthImage expands run-length encoding to original', () => {
            const result = decodeRunLengthImage(encoding)
            const isAsExpected = result.every((e, i) => e === original[i])
            expect(isAsExpected).toBeTruthy()
        })

        it('sliceRunLengthImage returns a slice of a run-length encoding', () => {
            const result = sliceRunLengthImage(encoding, 1, 5)
            console.log(result)
        })
    })

    describe('getting forbidden points array', () => {
        it('gets forbidden points referring to a callback', () => {
            const blackIsForbidden = (col) => {
                const isSortOfBlack = compareArrayWithTolerance(black, col, 15)
                return !isSortOfBlack
            }

            const forbiddenPoints = getForbiddenPoints(testImgData.data, 25, blackIsForbidden)

            expect(forbiddenPoints.length).toBeGreaterThanOrEqual(20)
            expect(forbiddenPoints.length).toBeLessThanOrEqual(28)
        })

        it('can get a fuzzy selection of forbidden points', () => {
            const forbiddenPoints = getFuzzyForbiddenPoints(testImgData.data, 25, [white, red, blue, green], 15)
            expect(forbiddenPoints.length).toBeGreaterThanOrEqual(20)
            expect(forbiddenPoints.length).toBeLessThanOrEqual(28)
        })
    })
})
